from __future__ import print_function

__author__ = 'cpaulson'
import pyKriging
from pyKriging.krige import kriging
from pyKriging.samplingplan import samplingplan
import numpy as np

# The Kriging model starts by defining a sampling plan, we use an optimal Latin Hypercube here
sp = samplingplan(2)
X = sp.optimallhc(15)

# print(pyKriging.testfunctions().linear(X))
# print(pyKriging.testfunctions().squared(X))
# print(pyKriging.testfunctions().cubed(X))
# print(pyKriging.testfunctions().branin(X))
# print(pyKriging.testfunctions().paulson(X))
# print(pyKriging.testfunctions().runge(X))
# print(pyKriging.testfunctions().stybtang(X))
# print(pyKriging.testfunctions().curretal88exp(X))
# print(pyKriging.testfunctions().cosine(X))
# print(pyKriging.testfunctions().rastrigin(X))
# print(pyKriging.testfunctions().rosenbrock(X))

# Next, we define the problem we would like to solve
testfun = pyKriging.testfunctions().branin

# We generate our observed values based on our sampling plan and the test function
y = testfun(X)

print('Setting up the Kriging Model')

# X = np.array([[1, 2, 3, 4], [1, 2, 3, 4]])
# y = np.array([2, 4, 6, 8])

# Now that we have our initial data, we can create an instance of a kriging model
k = kriging(X, y, testfunction=testfun, name='simple', testPoints=250)
# k = kriging(X, y, name='simple', testPoints=250)
# k.plot()
k.train(optimizer='ga')
# k.plot()
k.snapshot()

for i in range(0):
    newpoints = k.infill(1)
    for point in newpoints:
        print(('Adding point {}'.format(point)))
        k.addPoint(point, testfun(point)[0])
    k.train()
    k.snapshot()

# #And plot the model

print('Now plotting final results...')
k.plot()
