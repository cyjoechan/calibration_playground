import numpy as np
from matplotlib import pyplot as plt
from scipy.ndimage import filters
import cv2
from scipy.stats import gaussian_kde


class SimImg(object):
    def __init__(self, img_size=512, coord=[200, 300, 0]):
        self.img_size = img_size
        self.x = coord[0]
        self.y = coord[1]
        self.z = None
        self.truez = coord[2]
        self.circle = []
        self.img = [[]]
        self.beam_diameter = None

    def sim_beamsize(self, z):
        self.beam_diameter = ((self.truez - z) ** 2) / 10 + 10
        return self.beam_diameter

    def sim_image(self, z):
        self.z = z
        self.img = np.zeros((self.img_size[0][1], self.img_size[1][1]))
        laser_point = (int(self.x), int(self.y))
        self.img[laser_point] = 1
        self.beam_diameter = self.sim_beamsize(z)
        self.img = filters.gaussian_filter(self.img, (self.beam_diameter, self.beam_diameter))
        self.img = normalize_img(self.img)
        return self.img

    def show(self):
        if len(self.circle) != 0:
            img = self.img
            cv2.circle(img, (int(self.circle[1]), int(self.circle[0])), int(self.circle[2]), 255, 2)
        plt.imshow(img)
        plt.xlabel("y")
        plt.ylabel("x")
        if len(self.circle) != 0:
            plt.title(
                'z: ' + '%.2f' % self.z + '\nsimulate beam size: ' + '%.2f' % self.beam_diameter + '\nfitted beam size: ' + '%.2f' %
                self.circle[2])
        plt.show()


def normalize_img(img):
    """
    :param img: 2D array of an image
    :return: grey level of each pixel normalized between [0,255]
    """
    img = img / np.max(img) * (2 ** 8 - 1)
    img = np.clip(img, 0, 2 ** 8 - 1)
    img = img.astype('uint8')
    return img


def trans_nl_3d(coords, scale, angles, translation, nonlinearity=0):
    """
    simulate 3D non-linear transform
    """
    coords_after_trans = []
    # rotation
    angles_radian = np.array(angles) / 360 * (2 * np.pi)  # angle to radian
    c, s = np.cos(angles_radian), np.sin(angles_radian)
    if np.shape(coords)[0] == 1:
        coords=[coords]
    for coord in coords:
        # nonlinear
        r_sq = np.sum(np.array(coord) ** 2)
        coord = np.append(coord, 1)
        nonlinear_mat = np.vstack((np.identity(3), np.ones(3) * r_sq * nonlinearity))
        coord = np.dot(coord, nonlinear_mat)
        # scaling
        scaling = [[scale[0], 0, 0], [0, scale[1], 0], [0, 0, scale[2]]]
        coord = np.dot(coord, scaling)
        coord = np.append(coord, 1)  # padding for translation
        rotation = [[[1., 0, 0],
                     [0, c[0], -s[0]],
                     [0, s[0], c[0]]],
                    [[c[1], 0, -s[1]],
                     [0, 1, 0],
                     [s[1], 0, c[1]]]]
        tm = np.hstack((np.dot(rotation[0], rotation[1]), np.vstack(translation)))
        #     print(tm.T)
        coord = np.dot(coord, tm.T)
        coords_after_trans.append(coord)
    coords_after_trans = np.array(coords_after_trans)

    return coords_after_trans


def generate_points_3d(points_1, show=0, noise_percentage=0, angles=[3, 6], translation1 = [310, 330, 320]):
    """
    apply non-linear 3D transformation to a set of coords
    """
    range = np.amax(points_1) - np.amin(points_1)
    noise = np.multiply((np.random.rand(np.shape(points_1)[0], 3) - 0.5) * 2 * noise_percentage, range)
    # points_1 = mesh_3d(number_of_point, size_of_img)
    scaling1 = [1, 1, 1]
    nonlinearity = 0.0001  # 0.0001=>75 at(500,500,500)
    points_2_ideal = trans_nl_3d(points_1, scaling1, angles, translation1, nonlinearity)
    points_2_with_noise = points_2_ideal + noise
    if show != 0:
        distances = np.sqrt(np.sum(noise ** 2, 1))
        plt.hist(distances, bins=20)
        plt.xlabel('noise')
        plt.ylabel('number of points')
        plt.show()

        if noise_percentage != 0:
            kernel = gaussian_kde(distances)
            x_kde = np.linspace(0, max(distances), num=1000)
            y_kde = kernel.evaluate(np.linspace(0, max(distances), num=1000)) / kernel.n * kernel._norm_factor
            plt.scatter(x_kde, y_kde, c='b', s=1)
            plt.xlabel('distance error')
            plt.ylabel('density')
            plt.show()

        fig = plt.figure(figsize=(8, 5))
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(points_2_with_noise[:, 0], points_2_with_noise[:, 1], points_2_with_noise[:, 2], c='r',
                   label='noisy transform', s=show)
        ax.scatter(points_2_ideal[:, 0], points_2_ideal[:, 1], points_2_ideal[:, 2], c='b', label='ideal transform',
                   s=show)
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        ax.legend()
        plt.show()
    return points_2_with_noise, points_2_ideal
