function mini_joe_calib()
%     setenv('PATH', ['C:\ProgramData\Anaconda3\envs\holo;C:\ProgramData\Anaconda3\envs\holo\Library\mingw-w64\bin;C:\ProgramData\Anaconda3\envs\holo\Library\usr\bin;C:\ProgramData\Anaconda3\envs\holo\Library\bin;C:\ProgramData\Anaconda3\envs\holo\Scripts;C:\ProgramData\Anaconda3\envs\holo\bin;C:\ProgramData\Anaconda3\condabin;C:\Program Files\Python37\Scripts\;C:\Program Files\Python37\;' getenv('PATH')]);
%     python_client_path = 'C:\Users\cyjoechan\Documents\calibration_playground\zmq_practice\zmq_client.py';
    python_client_path = 'C:\Users\cyjoechan\Documents\calibration_playground\holo_project\holographics\holoclient_mes.py';
    function setx(x)
        if x<200 && x>-200
            simple_out('GalvoXcmd', x, 'save');
        else
            disp('out of x galvo range')
        end
    end
    function sety(y)
        if y<200 && y>-200
            simple_out('GalvoYcmd', y, 'save');
        else
            disp('out of y galvo range')
        end
    end
    function Zlevel = getz()
        f = mesdevice('get', 'ObjectiveArm');
        Zlevel = get(f, 'position');
    end
    function setz(Zlevel)
        if Zlevel>-12000 || Zlevel<-14000
            disp(['out of z range: ' num2str(Zlevel)])
        else
            f = mesdevice('get', 'ObjectiveArm');
            res = set(f, 'position', Zlevel);
            pause(1)
            if res == 0
                disp('Couldn''t set obj position')
            elseif res == 1
                disp('Set obj position')
            else
                disp('unknown return when setting objective')
            end
        end
    end
    function mini_joe_testing(x,y,z)
        simple_out('Sh1', 0, 'save');
        simple_out('Sh2', 0, 'save');
       python(python_client_path, num2str(0), num2str(0),num2str(0))
        simple_out('Sh1', 1, 'save');
        simple_out('Sh2', 0, 'save');
        for k = 1:size(z, 2)
            setz(z(k))
            for i = 1:size(x, 2)
                setx(x(i))
                for j = 1:size(y, 2)
                    sety(y(j))
                    disp([round(x(i)) round(y(j)) round(z(k))])
                    python(python_client_path, num2str(round(x(i))), num2str(round(y(j))),num2str(round(getz())))
                end
            end
        end
%         python(python_client_path, num2str(x), num2str(y),num2str(z))
    end
    function generate_mesh(n_pts,size)
         python(python_client_path, 'MESH',num2str(n_pts), num2str(size))
    end

    function autofocus(holo_z)
        simple_out('Sh1', 0, 'save');
        simple_out('Sh2', 1, 'save');
        base_z = getz()
        while 1
            current_z=num2str(getz())
            new_z=str2num(python(python_client_path, 'AUTOFOCUS', num2str(holo_z), current_z));
            if abs(new_z-base_z)<500 % to prevent the obj move too much
                setz(new_z)
            end
            if new_z > -1 % 0 means done focusing
                break;
            end
        end
        set(f, 'position', base_z);
    end

%  z_num=str2num(z);
%  disp(z_num)
f = mesdevice('get', 'ObjectiveArm');
set(f, 'position', -12950);
holo_z=50
autofocus(holo_z)
% generate_mesh(4,100)
%     n_pts=4
%     z_focus = getz();
%     
%     x=linspace(-70,100,n_pts);
%     y=linspace(-60,70,n_pts);
%     z=linspace(z_focus-80,z_focus+80,n_pts);
%     tic
%     mini_joe_testing(x,y,z)
%     mini_joe_testing(10,20,30)
%     toc
%     setx(-70)
%     sety(-60)
%     setz(-20943)
    
    
    
    
%     system(['python ' python_client_path  num2str(x(i))  num2str(y(j)])

end