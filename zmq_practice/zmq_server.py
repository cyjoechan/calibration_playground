import time
import zmq
import pf_pb2

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://*:51233")

while True:
    #  Wait for next request from client
    # message = socket.recv()
    # print("Received request: %s" % message)
    data= socket.recv()
    img_meta = pf_pb2.ImageMeta()
    img_meta.ParseFromString(data)

    #  Do some 'work'
    print(str(int(img_meta.Zlevel)) + ' ' + str(int(img_meta.frame_num)) + ' ' + str(int(img_meta.duration)))
    time.sleep(4)

    #  Send reply back to client
    # socket.send(b"World")
    socket.send_string(str(img_meta.Zlevel))
