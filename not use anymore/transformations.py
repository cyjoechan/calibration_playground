import numpy as np
from scipy import optimize


def applytrans(mat, trans):
    """
    :param mat: 2xN np array (x as column 1, y as column 2)
    :param trans: 3x3 transformation matrix
    """
    amat = np.hstack((mat, np.ones((np.shape(mat)[0], 1))))
    return np.dot(trans, amat.T)[:2, :].T


def pad_trans(trans):
    return np.vstack((trans, [0, 0, 1]))


def reprojection_error(data1, data2, trans):
    return rms(applytrans(data1, pad_trans(trans)), data2)


def fit_trans(data1, data2):
    def do_trans(trans):
        trans = trans.reshape((2, 3))
        return reprojection_error(data1, data2, trans)

    res = optimize.minimize(do_trans, np.identity(3)[:2, :])
    return res.x.reshape((2, 3)), do_trans(res.x)


def rms(data1, data2):
    return ((np.asarray(data1) - np.asarray(data2)) ** 2).mean() ** .5
