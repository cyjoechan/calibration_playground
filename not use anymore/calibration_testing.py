import numpy as np
from matplotlib import pyplot as plt
import scipy.stats
import transformations


def findcenter(self, img, blurRadius=211):
    assert blurRadius % 2 == 1, "blurRadius must be odd!"

    img = img.copy()
    if self.background is not None:
        img = img.astype('int32')
        img -= self.background
        img = np.clip(img, 0, 2 ** 8)
        img = img.astype('uint8')

    img = cv2.GaussianBlur(img, (blurRadius,) * 2, 0, 0)
    centroid_int = np.unravel_index(img.argmax(), img.shape)
    # TODO could detect a poor fit, and raise an Exception
    centroid_int = (centroid_int[1], centroid_int[0])

    return centroid_int

def zcalib(Zlevels, Zobjs):
    Zlevels = np.asarray(Zlevels)
    Zobjs = np.asarray(Zobjs)
    coefs = np.polyfit(Zobjs, Zlevels, deg=5)
    zmin = Zobjs.min() - .1 * (Zobjs.max() - Zobjs.min())
    zmax = Zobjs.max() + .1 * (Zobjs.max() - Zobjs.min())
    x = np.linspace(zmin, zmax, 500)
    plt.scatter(Zobjs, Zlevels)
    plt.plot(x, np.polyval(coefs, x))
    plt.xlabel("Z obj")
    plt.ylabel("Z level")
    # plt.xlim([zmin, zmax])
    # plt.ylim([zmin, zmax])
    plt.gca().set_aspect('equal')
    plt.plot(x, x, c='k')

    slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(Zobjs, Zlevels)
    print("linear fit r-value: {0}".format(r_value))
    plt.plot(x, slope * x + intercept, 'g-')

    plt.show()


# zcalib([0,15, 35], [0, 10, 20])

def xycalib(data1, data2):
#     circle_centers = [findcenter(img, 155) for img in self.circle_images]
#     holo_imgs = [grab_point(pos[0], pos[1], 0) for pos in self.circle_positions]
#
#     holo_centers = [findcenter(img, 431) for img in holo_imgs]
#
#     calib1, err1 = transformations.fit_trans(np.asarray(circle_positions), np.asarray(circle_centers))
#     calib2, err2 = transformations.fit_trans(np.asarray(holo_centers), np.asarray(circle_positions))
    data1 = np.asarray(np.array(data1))
    data2 = np.asarray(np.array(data2))
    calib1, err1 = transformations.fit_trans(data1, data2)
    return calib1

# for number_of_point in range(4,20):
# for number_of_point in [10,20,30,40,50,60,70,80,90,100]:
for number_of_point in [10]:
    noises=np.zeros(20)
    for j in range(20):
        size_of_img = 1000
        noise_percentage = 0.002
        data1 = np.random.rand(number_of_point, 2) * size_of_img
        # data1 = [[2, 3], [6, 4], [4, 6], [5, 2]]
        # ideal_trans = [[0.95, 0.1, 0], [-0.1, 0.99, 0]]
        ideal_trans = [[0.95, 0.1, 0], [-0.1, 0.99, 0]]
        # data2 = [[1.1, 3], [3.7, 5], [1.8, 6.2], [2.9, 1.2]]
        # noise = (np.random.rand(*np.shape(data1))-0.5)*(size_of_img*noise_percentage)
        noise = np.multiply((np.random.rand(*np.shape(data1)) - 0.5) * (size_of_img * noise_percentage), data1)
        # print('noise')
        # print(noise)
        ideal_data2 = transformations.applytrans(data1, ideal_trans)
        data2 = transformations.applytrans(data1, ideal_trans) + noise
        calib1 = xycalib(data1, data2)


        data1 = np.asarray(np.array(data1))
        data2 = np.asarray(np.array(data2))
        data1_dot_calib1 = transformations.applytrans(data1, calib1)
        noises[j] = transformations.rms(ideal_data2, data1_dot_calib1) / transformations.rms(data2, data1_dot_calib1)
    #     plt.subplot(121)
    #     plt.scatter(number_of_point, noises[j], color='red')
    #
    #
    # plt.subplot(122)
    # plt.scatter(number_of_point, np.mean(noises), color='blue')
    # plt.errorbar(number_of_point, np.mean(noises), np.std(noises), color='blue')

    plt.title('number_of_point= ' + str(number_of_point))
    plt.scatter(data1[:, 0], data1[:, 1], color='red')
    plt.scatter(data2[:, 0], data2[:, 1], color='blue')
    plt.scatter(data1_dot_calib1[:, 0], data1_dot_calib1[:, 1], color='green')
    plt.scatter(ideal_data2[:, 0], ideal_data2[:, 1], color='purple')
    plt.legend(['data1', 'data2_with_noise', 'trans_with _noise', 'data2_without_noise'])
    plt.show()

# plt.xlabel("number of points for calibration")
# plt.subplot(121)
# plt.ylabel("ratio of rms")
# plt.show()
