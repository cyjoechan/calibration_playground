import os
import numpy as np
import matplotlib.pyplot as plt


def sim_z(truez, z):
    img = np.zeros((512, 512))
    laser_point = (200, 300)
    img[laser_point] = 1
    beam_diameter = ((truez - z) ** 2) + 10
    return beam_diameter
    # return filters.gaussian_filter(img, (beam_diameter, beam_diameter))  # scale blur np.abs(truez-z)


plt.style.use('ggplot')
# Data taken from https://blog.dominodatalab.com/fitting-gaussian-process-models-
# X, y = np.array([[-10, sim_image(1, -10)], [-7, sim_image(1, -7)], [-4, sim_image(1, -4)], [-1, sim_image(1, -1)], [2, sim_image(1, 2)], [5, sim_image(1, 5)], [8, sim_image(1, 8)]]).T
X, y = np.array([[-100, sim_z(10, -100)], [-70, sim_z(10, -70)], [-40, sim_z(10, -40)], [-10, sim_z(10, -10)], [20, sim_z(10, 20)], [50, sim_z(10, 50)], [80, sim_z(10, 80)]]).T
from pykrige import OrdinaryKriging

X_pred = np.linspace(-80, 100, 200)
# pykrige doesn't support 1D data for now, only 2D or 3D
# adapting the 1D input to 2D
uk = OrdinaryKriging(X, np.zeros(X.shape), y, variogram_model='gaussian', )
y_pred, y_std = uk.execute('grid', X_pred, np.array([0.]))
y_pred = np.squeeze(y_pred)
print(np.min(y_pred))
y_std = np.squeeze(y_std)
fig, ax = plt.subplots(1, 1, figsize=(10, 4))
ax.scatter(X, y, s=40, label='Input data')
# ax.plot(X_pred, y_pred, label='Predicted values')
# ax.fill_between(X_pred, y_pred - 3 * y_std, y_pred + 3 * y_std, alpha=0.3, label='Confidence interval')
ax.plot(X_pred, y_pred, label='Predicted values')
ax.legend(loc=9)
ax.set_xlabel('z level')
ax.set_ylabel('beam diameter')
# ax.set_xlim(-6, 6)
# ax.set_ylim(-2.8, 3.5)
if 'CI' not in os.environ:
    # skip in continous integration
    plt.show()

