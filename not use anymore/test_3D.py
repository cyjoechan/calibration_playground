import pyKriging
from pyKriging.krige import kriging
from pyKriging.samplingplan import samplingplan
from pyKriging.regressionkrige import regression_kriging

import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.stats import gaussian_kde


def t_3D(coords, scale, angles, translation, nonlinearity=0):
    #     simulate 3D non-linear transform
    coords_after_trans = []
    # rotation
    angles_radian = np.array(angles) / 360 * (2 * np.pi)  # angle to radian
    c, s = np.cos(angles_radian), np.sin(angles_radian)
    for coord in coords:
        # nonlinear
        r_sq = np.sum(np.array(coord) ** 2)
        coord = np.append(coord, 1)
        nonlinear_mat = np.vstack((np.identity(3), np.ones((3)) * r_sq * nonlinearity))
        coord = np.dot(coord, nonlinear_mat)
        # scaling
        scaling = [[scale[0], 0, 0], [0, scale[1], 0], [0, 0, scale[2]]]
        coord = np.dot(coord, scaling)
        coord = np.append(coord, 1)  # padding for translation
        #     print(coords)

        rotation = [[[1., 0, 0],
                     [0, c[0], -s[0]],
                     [0, s[0], c[0]]],
                    [[c[1], 0, -s[1]],
                     [0, 1, 0],
                     [s[1], 0, c[1]]]]
        tm = np.hstack((np.dot(rotation[0], rotation[1]), np.vstack(translation)))
        #     print(tm.T)
        coord = np.dot(coord, tm.T)
        coords_after_trans.append(coord)
    coords_after_trans = np.array(coords_after_trans)

    return coords_after_trans


def mesh_3D(size_of_img, number_of_point):
    points_1 = np.linspace(size_of_img * 0.3, size_of_img * 0.7, num=number_of_point)
    points_1 = np.meshgrid(points_1, points_1, points_1)
    points_1 = np.reshape(np.ravel(points_1), (3, number_of_point ** 3)).T
    return points_1


def generate_points_3D(number_of_point, show=False, noise_percentage=0):
    # apply 3D mesh to 3D transform
    size_of_img = 500
    noise = (np.random.rand(number_of_point ** 3, 3) - 0.5) * 2 * (size_of_img * noise_percentage)
    if show:
        plt.hist(np.sqrt(np.sum((noise) ** 2, 1)), bins=20)
        plt.xlabel('noise')
        plt.ylabel('number of points')
        plt.show()
    #     points_1 = np.linspace(size_of_img * 0.1, size_of_img * 0.9, num=number_of_point)
    #     points_1 = np.meshgrid(points_1, points_1,points_1)
    #     points_1 = np.reshape(np.ravel(points_1), (3, number_of_point ** 3)).T
    points_1 = mesh_3D(size_of_img, number_of_point)
    scaling1 = [1, 1, 1]
    angles1 = [3, 6]
    translation1 = [310, 330, 320]
    nonlinearity = 0.0001  # 0.0001=>75 at(500,500,500)
    points_2_ideal = t_3D(points_1, scaling1, angles1, translation1, nonlinearity)
    points_2_with_noise = points_2_ideal + noise
    return points_1, points_2_with_noise, points_2_ideal


def kriging_3D_predict(points_1, points_2_fitting, points_1_pred, points_2_ideal_pred=[], thetamin=1e-5, thetamax=1e0,
                       pmin=2, pmax=2, show=False):
    # build kriging model and predict
    #     krig_model=[]
    krig_pred = [[], [], []]
    for i in range(3):
        #         print('building Kriging model kx...')
        krig_model = regression_kriging(points_1, points_2_fitting[:, i], testPoints=250)
        krig_model.thetamin = thetamin
        krig_model.thetamax = thetamax
        krig_model.pmin = pmin
        krig_model.pmax = pmax
        krig_model.train(optimizer='ga')
        krig_model.snapshot()
        #         print('Predicting kx...')

        for pt in points_1_pred:
            krig_pred[i].append(krig_model.predict(pt))
    krig_pred = np.array(krig_pred)
    # ######
    #         print('building Kriging model ky...')
    #         k2 = regression_kriging(points_1, points_2_fitting[:, 1], testPoints=250)
    #         k2.thetamin = thetamin
    #         k2.thetamax = thetamax
    #         k2.pmin = pmin
    #         k2.pmax = pmax
    #         k2.train(optimizer='ga')
    #         k2.snapshot()
    #         print('Predicting ky...')
    #         ky=[]
    #         for pt in points_1_pred:
    #             ky.append(k2.predict(pt))
    #         ky=np.array(ky)
    #         ######
    #         print('building Kriging model kz...')
    #         k3 = regression_kriging(points_1, points_2_fitting[:, 2], testPoints=250)
    #         k3.thetamin = thetamin
    #         k3.thetamax = thetamax
    #         k3.pmin = pmin
    #         k3.pmax = pmax
    #         k3.train(optimizer='ga')
    #         k3.snapshot()
    #         print('Predicting kz...')
    #         kz=[]
    #         for pt in points_1_pred:
    #             kz.append(k3.predict(pt))
    #         kz=np.array(kz)

    if show != 0:
        # ############################################################
        fig = plt.figure(figsize=(8, 5))
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(points_2_ideal_pred[:, 0], points_2_ideal_pred[:, 1], points_2_ideal_pred[:, 2], c='b',
                   label='ideal_transformed', s=show)
        ax.scatter(krig_pred[0], krig_pred[1], krig_pred[2], c='r', label='kriging', s=show)
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        ax.legend()
        plt.show()
        # ############################################################
        distance_error_kde(krig_pred.T, points_2_ideal_pred, show=True)
    #         distances=np.sqrt(np.sum((np.array(krig_pred).T-points_2_ideal_pred)**2,1))
    #         # print(distances)
    #         plt.hist(distances, bins='auto')
    #         plt.xlabel('distance error')
    #         plt.ylabel('number of points')
    #         plt.show()
    #         kernel = gaussian_kde(distances)
    #         plt.scatter(np.linspace(0, max(distances), num=1000), kernel.evaluate(np.linspace(0, max(distances), num=1000))/(kernel.n / kernel._norm_factor),c='b',s=1)
    #         plt.show()
    return krig_pred.T


def distance_error_kde(data1, data2, show=False):
    distances = np.sqrt(np.sum((data1 - data2) ** 2, 1))
    kernel = gaussian_kde(distances)
    x_kde = np.linspace(0, max(distances), num=1000)
    y_kde = kernel.evaluate(np.linspace(0, max(distances), num=1000)) / kernel.n * kernel._norm_factor
    if show != False:
        plt.hist(distances, bins='auto')
        plt.xlabel('distance error')
        plt.ylabel('number of points')
        plt.show()
        plt.scatter(x_kde, y_kde, c='b', s=1)
        plt.show()
    return x_kde, y_kde


## initial 8 to 64 f(x,y,z)=z kriging for auto focus
import time
from mic_simulator import SimImg
from mic_simulator import AutoFocus

size_of_img = 500

##############################################################
# generate 8 points for guessing z coords of the 64 points
error_input = []
# error_input.append([])
error_wo_sim = []
error_w_sim = []
build_time = []
pred_time = []
noises = [0.3]
number_of_points = [8]
# noises=[0,0.01,0.1]
# number_of_points=[2,4,7]

for i in range(np.shape(number_of_points)[0]):
    error_input.append([])
    error_wo_sim.append([])
    build_time.append([])
    pred_time.append([])
    number_of_pt = number_of_points[i]
    for j in range(np.shape(noises)[0]):
        start = time.time()
        noise = noises[j]
        print('\n\nnumber_of_point: ' + str(number_of_pt))
        print('noise: ' + str(noise))
        # noise=0.05
        imaging_8, camera_8_noisy, camera_8_ideal = generate_points_3D(number_of_point=2, noise_percentage=noise)
        imaging_64, camera_64_noisy, camera_64_ideal = generate_points_3D(number_of_point=number_of_pt,
                                                                          noise_percentage=noise, show=True)
        if noise != 0:
            input_kde = distance_error_kde(camera_64_noisy, camera_64_ideal, show=False)
            error_input[i].append(input_kde[0][np.argmax(input_kde[1])])
        else:
            error_input[i].append(0)
        initial_guess = kriging_3D_predict(imaging_8, camera_8_noisy, imaging_64, camera_64_ideal, show=64)

        #     # scan all points and get accurate (x,y,z)
        #     kx_f=[]
        #     ky_f=[]
        #     kz_f=[]
        #     # initial_guess = np.vstack((kx_initial_guess, ky_initial_guess, kz_initial_guess)).T
        #     for i in range(64):
        #         pt1=SimImg(size_of_img*2,points_2_noisy[i][0],points_2_noisy[i][1],points_2_noisy[i][2])
        #         getfocus_pt1=AutoFocus(pt1.sim_image, -15+initial_guess[i][2],15+initial_guess[i][2],img_size=size_of_img*2)
        #         kz_f.append(getfocus_pt1.kriging_dct_focus(4))
        #         circ=getfocus_pt1.find_circle(kz_f[-1])
        #         kx_f.append(circ[0])
        #         ky_f.append(circ[1])
        #     kx_f=np.array(kx_f)
        #     ky_f=np.array(ky_f)
        #     kz_f=np.array(kz_f)
        #     fine_camera_64=np.vstack((kx_f, ky_f, kz_f)).T
        end = time.time()
        print('\nbuild time: ' + str(end - start))
        build_time[i].append(end - start)
        start = time.time()

        # apply kriging and do a fine fitting
        imaging_8000, camera_8000_noisy, camera_8000_ideal = generate_points_3D(number_of_point=20, noise_percentage=0)
        print('\nwithout image simulation')
        fine_predict_wo_sim = kriging_3D_predict(imaging_64, camera_64_noisy, imaging_8000, camera_8000_ideal, show=2)
        output_kde = distance_error_kde(fine_predict_wo_sim, camera_8000_ideal, show=False)
        error_wo_sim[i].append(output_kde[0][np.argmax(output_kde[1])])

        end = time.time()
        print('\npredict time: ' + str(end - start))
        pred_time[i].append(end - start)

    #     print('\nwith image simulation')
    #     fine_predict_w_sim = kriging_3d_predict(points_1, fine_camera_64, imaging_8000,camera_8000_ideal, show=2)
    #     error_w_sim.append(distance_kde(fine_predict_w_sim,camera_8000_ideal,show=False))

# #################
# # error KDE (histogram) of different noise level in one graph
# error_wo_sim = np.array(error_wo_sim)
# error_w_sim = np.array(error_w_sim)
# n=np.shape(noises)[0]

# color=iter(plt.cm.rainbow(np.linspace(0,1,n)))
# for i in range(1,n):
#     plt.scatter(error_input[i][0],error_input[i][1],c=[next(color)],s=1,label='noise: '+str(int(noises[i]*100))+'%')
# plt.xlabel('distance noise (input)')
# plt.ylabel('Kernel Density (KDE)')
# plt.legend()
# plt.show()

# color=iter(plt.cm.rainbow(np.linspace(0,1,n)))
# for i in range(n):
#     plt.scatter(error_wo_sim[i][0],error_wo_sim[i][1],c=[next(color)],s=1,label='noise: '+str(int(noises[i]*100))+'%')
# plt.xlabel('distance error (output)')
# plt.ylabel('Kernel Density (KDE)')
# plt.legend()
# plt.show()

# # for i in range(n):
# #     plt.scatter(error_w_sim[i][0],error_w_sim[i][1],c=[next(color)],s=1,label='noise: '+str(int(noises[i]*100))+'%')
# # plt.xlabel('distance error (output)')
# # plt.ylabel('Kernel Density (KDE)')
# # plt.legend()
# # plt.show()



ratio=np.divide(error_input,error_wo_sim)
color=iter(plt.cm.rainbow(np.linspace(0,1,np.shape(number_of_points)[0])))
for i in range(np.shape(number_of_points)[0]):
#     for j in range(np.shape(noises)[0]):
    plt.scatter(noises,error_input[i],c=[next(color)],label='number_of_points: '+str(int(number_of_points[i]**3)))
plt.xlabel('input noise level')
plt.ylabel('mode of noise')
plt.title('input noise')
plt.legend(bbox_to_anchor=(1.1, 1.05))
plt.show()

color=iter(plt.cm.rainbow(np.linspace(0,1,np.shape(number_of_points)[0])))
for i in range(np.shape(number_of_points)[0]):
#     for j in range(np.shape(noises)[0]):
    plt.scatter(noises,error_wo_sim[i],c=[next(color)],label='number_of_points: '+str(int(number_of_points[i]**3)))
plt.xlabel('input noise level')
plt.ylabel('mode of noise')
plt.title('output noise')
# plt.legend()
plt.show()

color=iter(plt.cm.rainbow(np.linspace(0,1,np.shape(number_of_points)[0])))
for i in range(np.shape(number_of_points)[0]):
#     for j in range(np.shape(noises)[0]):
    plt.scatter(noises,ratio[i],c=[next(color)],label='number_of_points: '+str(int(number_of_points[i]**3)))
plt.xlabel('input noise level')
plt.ylabel('mode of noise')
plt.title('ratio of in/out')
# plt.legend()
plt.show()
#####################################################
error_input=np.array(error_input)
error_wo_sim=np.array(error_wo_sim)
ratio=np.array(ratio)
build_time=np.array(build_time)
pred_time=np.array(pred_time)


color=iter(plt.cm.rainbow(np.linspace(0,1,np.shape(number_of_points)[0])))
for i in range(np.shape(noises)[0]):
    plt.scatter(number_of_points,error_input.T[i],c=[next(color)],label='noise: '+str(int(noises[i]*100))+'%')
plt.xlabel('number_of_points')
plt.ylabel('mode of noise')
plt.title('input noise')
plt.legend(bbox_to_anchor=(1.1, 1.05))
plt.xticks(number_of_points, ['2^3','3^3','4^3','5^3','6^3','7^3','8^3',])
plt.show()

color=iter(plt.cm.rainbow(np.linspace(0,1,np.shape(number_of_points)[0])))
for i in range(np.shape(noises)[0]):
    plt.scatter(number_of_points,error_wo_sim.T[i],c=[next(color)],label='noise: '+str(int(noises[i]*100))+'%')
plt.xlabel('number_of_points')
plt.ylabel('mode of noise')
plt.title('output noise')
plt.xticks(number_of_points, ['2^3','3^3','4^3','5^3','6^3','7^3','8^3',])
# plt.legend()
plt.show()

color=iter(plt.cm.rainbow(np.linspace(0,1,np.shape(number_of_points)[0])))
for i in range(np.shape(noises)[0]):
    plt.scatter(number_of_points,ratio.T[i],c=[next(color)],label='noise: '+str(int(noises[i]*100))+'%')
plt.xlabel('number_of_points')
plt.ylabel('mode of noise')
plt.title('ratio of in/out')
plt.xticks(number_of_points, ['2^3','3^3','4^3','5^3','6^3','7^3','8^3',])
# plt.legend()
plt.show()

# run time
color=iter(plt.cm.rainbow(np.linspace(0,1,np.shape(number_of_points)[0])))
for i in range(np.shape(noises)[0]):
    plt.scatter(number_of_points,build_time.T[i],c=[next(color)],label='noise: '+str(int(noises[i]*100))+'%')
#     plt.annotate('(%s)' % run_time.T[i][0], xy=np.array([number_of_points[i],run_time.T[i][0]]), textcoords='data')
plt.xlabel('number_of_points')
plt.ylabel('build_time')
plt.title('build_time')
plt.legend()
plt.xticks(number_of_points, ['2^3','4^3','6^3'])
plt.show()

color=iter(plt.cm.rainbow(np.linspace(0,1,np.shape(number_of_points)[0])))
for i in range(np.shape(noises)[0]):
    plt.scatter(number_of_points,pred_time.T[i],c=[next(color)],label='noise: '+str(int(noises[i]*100))+'%')
#     plt.annotate('(%s)' % run_time.T[i][0], xy=np.array([number_of_points[i],run_time.T[i][0]]), textcoords='data')
plt.xlabel('number_of_points')
plt.ylabel('pred_time')
plt.title('pred_time')
plt.legend()
plt.xticks(number_of_points, ['2^3','4^3','6^3'])
plt.show()


