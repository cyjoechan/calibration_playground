import numpy as np
from scipy import optimize


class Transformation(object):
    def __init__(self, type_of_trans, data1, data2=[]):
        self.type_of_trans = type_of_trans
        self.data1 = data1
        self.data2 = data2

    def applytrans(self, trans, data1=None):
        if data1 is None:
            data1 = self.data1
        if self.type_of_trans == 'linear':
            """
            :param data1: 2xN np array (x as column 1, y as column 2)
            :param trans: 3x3 transformation matrix
            """
            amat = np.hstack((data1, np.ones((data1.shape[0], 1))))
            return np.dot(trans, amat.T)[:2, :].T
        elif self.type_of_trans == 'nonlinear':
            """
            :param data1: 2xN np array (x as column 1, y as column 2)
            :param trans: 3x4 transformation matrix
            """
            r = np.reshape(((data1[:, 0] ** 2 + data1[:, 1] ** 2) ** 0.5), (np.shape(data1)[0], 1))
            amat = np.hstack((data1, r))
            amat = np.hstack((amat, np.ones((np.shape(amat)[0], 1))))
            return np.dot(trans, amat.T)[:2, :].T
        else:
            return 0

    def pad_trans(self, trans):
        if self.type_of_trans == 'linear':
            return np.vstack((trans, [0, 0, 1]))
        elif self.type_of_trans == 'nonlinear':
            return np.vstack((trans, [0, 0, 0, 1]))
        else:
            return 0

    def rms(self, data1, data2):
        return ((np.asarray(data1) - np.asarray(data2)) ** 2).mean() ** .5

    def reprojection_error(self, trans):
        return self.rms(self.applytrans( self.pad_trans(trans),self.data1), self.data2)

    def do_trans(self, trans):
        if self.type_of_trans == 'linear':
            trans = trans.reshape((2, 3))
            return self.reprojection_error(trans)
        elif self.type_of_trans == 'nonlinear':
            trans = trans.reshape((2, 4))
            return self.reprojection_error(trans)
        else:
            return 0

    def fit_trans(self, type_of_trans):
        self.type_of_trans = type_of_trans
        if type_of_trans == 'linear':
            res = optimize.minimize(self.do_trans, np.identity(3)[:2, :])
            return res.x.reshape((2, 3)), self.do_trans(res.x)
        elif type_of_trans == 'nonlinear':
            res = optimize.minimize(self.do_trans, np.identity(4)[:2, :])
            return res.x.reshape((2, 4)), self.do_trans(res.x)
        else:
            return 0
