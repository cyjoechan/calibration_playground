from qtpy import QtCore, QtGui, QtWidgets
from desktopmagic import screengrab_win32 as grab
import numpy as np


class Screenshot(QtWidgets.QWidget):
    def __init__(self):
        super(Screenshot, self).__init__()

        self.screenshotLabel = QtWidgets.QLabel()
        self.screenshotLabel.setSizePolicy(QtWidgets.QSizePolicy.Expanding,
                                           QtWidgets.QSizePolicy.Expanding)
        self.screenshotLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.screenshotLabel.setMinimumSize(240, 160)

        self.createOptionsGroupBox()
        self.createButtonsLayout()

        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(self.screenshotLabel)
        mainLayout.addWidget(self.optionsGroupBox)
        mainLayout.addLayout(self.buttonsLayout)
        self.setLayout(mainLayout)

        self.shootScreen()
        self.delaySpinBox.setValue(1)

        self.setWindowTitle("Screenshot")
        self.resize(600, 400)

    def resizeEvent(self, event):
        scaledSize = self.originalPixmap.size()
        scaledSize.scale(self.screenshotLabel.size(), QtCore.Qt.KeepAspectRatio)
        if not self.screenshotLabel.pixmap() or scaledSize != self.screenshotLabel.pixmap().size():
            self.updateScreenshotLabel()

    def newScreenshot(self):
        self.newScreenshotButton.setDisabled(True)

        self.shootScreen()
        if self.repeatCheckBox.isChecked():
            self.repeatingLabel.setVisible(True)
            QtCore.QTimer.singleShot(self.delaySpinBox.value() * 1000, self.newScreenshot)
        else:
            self.repeatingLabel.setVisible(False)

    def saveScreenshot(self):
        format = 'png'
        initialPath = QtCore.QDir.currentPath() + "/untitled." + format

        fileName = QtWidgets.QFileDialog.getSaveFileName(self, "Save As",
                                                     initialPath,
                                                     "%s Files (*.%s);;All Files (*)" % (format.upper(), format))
        if fileName:
            self.originalPixmap.save(fileName, format)

    def shootScreen(self):
        if self.delaySpinBox.value() != 0:
            QtWidgets.qApp.beep()

        # Garbage collect any existing image first.
        self.originalPixmap = None
        rects = grab.getDisplayRects()
        a = np.array(grab.getRectAsImage(rects[self.screenSpinBox.value()]))
        QI = QtGui.QImage(a.data, a.shape[1], a.shape[0], a.strides[0],
                          QtGui.QImage.Format_RGB888)  # QtGui.QImage.Format_Indexed8
        self.originalPixmap = QtGui.QPixmap.fromImage(QI)

        self.updateScreenshotLabel()

        self.newScreenshotButton.setDisabled(False)

    def updateCheckBox(self):
        pass
        # if self.delaySpinBox.value() == 0:
        #     self.hideThisWindowCheckBox.setDisabled(True)
        # else:
        #     self.hideThisWindowCheckBox.setDisabled(False)

    def createOptionsGroupBox(self):
        self.optionsGroupBox = QtWidgets.QGroupBox("Options")

        self.screenSpinBox = QtWidgets.QSpinBox()
        self.screenSpinBox.setMinimum(0)
        self.screenSpinBox.setMaximum(len(grab.getDisplayRects()) - 1)
        # self.delaySpinBox.valueChanged.connect(self.updateCheckBox)

        self.screenSpinBoxLabel = QtWidgets.QLabel("Screen #:")

        self.delaySpinBox = QtWidgets.QSpinBox()
        self.delaySpinBox.setSuffix(" s")
        self.delaySpinBox.setMaximum(60)
        self.delaySpinBox.valueChanged.connect(self.updateCheckBox)

        self.delaySpinBoxLabel = QtWidgets.QLabel("Screenshot Delay:")

        self.repeatCheckBox = QtWidgets.QCheckBox("Repeat")

        self.repeatingLabel = QtWidgets.QLabel("Repeating!")

        optionsGroupBoxLayout = QtWidgets.QGridLayout()
        optionsGroupBoxLayout.addWidget(self.screenSpinBoxLabel, 0, 0)
        optionsGroupBoxLayout.addWidget(self.screenSpinBox, 0, 1)

        optionsGroupBoxLayout.addWidget(self.delaySpinBoxLabel, 1, 0)
        optionsGroupBoxLayout.addWidget(self.delaySpinBox, 1, 1)
        optionsGroupBoxLayout.addWidget(self.repeatCheckBox, 3, 0, 1, 2)
        optionsGroupBoxLayout.addWidget(self.repeatingLabel, 3, 1, 1, 2)
        self.repeatingLabel.setVisible(False)
        self.optionsGroupBox.setLayout(optionsGroupBoxLayout)

    def createButtonsLayout(self):
        self.newScreenshotButton = self.createButton("New Screenshot",
                                                     self.newScreenshot)

        self.saveScreenshotButton = self.createButton("Save Screenshot",
                                                      self.saveScreenshot)

        self.quitScreenshotButton = self.createButton("Quit", self.close)

        self.buttonsLayout = QtWidgets.QHBoxLayout()
        self.buttonsLayout.addStretch()
        self.buttonsLayout.addWidget(self.newScreenshotButton)
        self.buttonsLayout.addWidget(self.saveScreenshotButton)
        self.buttonsLayout.addWidget(self.quitScreenshotButton)

    def createButton(self, text, member):
        button = QtWidgets.QPushButton(text)
        button.clicked.connect(member)
        return button

    def updateScreenshotLabel(self):
        self.screenshotLabel.setPixmap(self.originalPixmap.scaled(
            self.screenshotLabel.size(), QtCore.Qt.KeepAspectRatio,
            QtCore.Qt.SmoothTransformation))


if __name__ == '__main__':
    import sys

    # import pylab
    # rects = grab.getDisplayRects()
    # i=grab.getRectAsImage(rects[0])
    # pylab.imshow(i);pylab.show()

    # grab.getScreenAsImage()
    #
    app = QtWidgets.QApplication(sys.argv)
    screenshot = Screenshot()
    screenshot.show()
    sys.exit(app.exec_())
