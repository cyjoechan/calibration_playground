from __future__ import division
from scipy.misc import imsave
from os import path
from holographics.svg_util import set_svg_bounds, svg_to_np, add_background
from qt_holo.metadata_parse import FemtonicsTifMeta


def rasterize_svg_with_meta(metadata, svg):
    svg = set_svg_bounds(svg, *metadata.image_rect)
    # dpi = 72 * metadata.width_um / metadata.Width
    return svg_to_np(svg, (512, 512))


def svgs_to_pngs(metadatapath, svgpaths, outdir='.', noalpha=False):
    assert path.exists(metadatapath)
    metadata = FemtonicsTifMeta(metadatapath)
    if issubclass(type(svgpaths), str):
        svgpaths = [svgpaths]

    for svgpath in svgpaths:
        with open(svgpath, 'rb') as f:
            svg = f.read()
        img = rasterize_svg_with_meta(metadata, svg)
        if noalpha:
            img = img[:, :, :3]
        outpath = path.join(outdir, path.splitext(path.split(svgpath)[-1])[0] + '.png')
        imsave(outpath, img)




if __name__ == '__main__':
    svgs_to_pngs(r'C:\holographic_project 1.3\QT_holo\stack_example\testforjoe.tif_metadata.txt',
                 r'C:\holo_project\holographics\raw_svgs\2015_03_24__01-44-49_frame1_1.00.svg')



