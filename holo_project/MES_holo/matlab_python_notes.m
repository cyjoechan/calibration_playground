%set python path
pyversion C:\ProgramData\Anaconda3\envs\holo\python.exe

%add to path
if count(py.sys.path, strcat(pwd, '\..\holographics')) == 0
    insert(py.sys.path,int32(0), strcat(pwd, '\..\holographics'));
end

holoclient_mes = py.importlib.import_module('holoclient_mes');

%to reload
clear classes
holoclient_mes = py.importlib.import_module('holoclient_mes');
py.importlib.reload(holoclient_mes);