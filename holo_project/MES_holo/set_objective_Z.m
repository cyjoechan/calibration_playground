function set_objective_Z(Zlevel)
    %Works, but only if focus isn't set on the Z control
    %Z value should be negative
    %Returns 1 if it worked, 0 otherwise

    %setZ('nohandwheel') - needs axis name as 2nd arg
    f = mesdevice('get', 'ObjectiveArm');
    res = set(f, 'position', Zlevel);
    %setZ('handwheel')
    if res == 0
        disp('Couldn''t set obj position')
    elseif res == 1
        disp('Set obj position')
    else
        disp('unknown return when setting objective')
    end
end