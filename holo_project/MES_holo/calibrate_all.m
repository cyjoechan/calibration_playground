% TODO update to new python syntax?
%#TODO add an easy way to debug
function calibrate_all()
    python_client_path = 'holoclient_mes.py';

    function position_galvo(x, y)
        simple_out('ScX', x, 'save');
        simple_out('ScY', y, 'save');
    end

    function Zlevel = get_objective_Z()
        f = mesdevice('get', 'ObjectiveArm');
        Zlevel = get(f, 'position');
    end

    function calibrate_correction_factor()
        simple_out('Sh1', 0, 'save');
        simple_out('Sh2', 1, 'save');
%        python(python_client_path, 'CALIBRATE_CORRECTION_FACTOR')
        simple_out('Sh2', 0, 'save');
    end

    function calibrate_XY(positions)
        simple_out('Sh2', 0, 'save');
        simple_out('Sh1', 0, 'save');
%        python(python_client_path, 'CALIBRATE_BACKGROUND')
        simple_out('Sh1', 1, 'save');
        for i = 1:size(positions, 1)
            position_galvo(positions(i, 1), positions(i, 2))
%            python(python_client_path, 'CALIBRATE_CIRCLE',...
%                num2str(positions(i, 1)), num2str(positions(i, 2)))
        end
        simple_out('Sh1', 0, 'save');
        simple_out('Sh2', 1, 'save');
%         python(python_client_path, 'CALIBRATE_RUN')
        simple_out('Sh1', 0, 'save');
        simple_out('Sh2', 0, 'save');
    end

    function calibrate_Z(Zlevels)
        %disp('Should start at imaging focal plane');
        simple_out('Sh1', 0, 'save');
        simple_out('Sh2', 1, 'save');
        base_Z = get_objective_Z();
        for i = 1:size(Zlevels, 2)
            %set_objective_Z(Zlevels(i) + base_Z) - doesn't work
            python(python_client_path, 'CALIBRATE_Z', num2str(Zlevels(i)))
            Z = get_objective_Z();
            python(python_client_path, 'CALIBRATE_Z_OBJ', num2str(Z-base_Z))
        end
        simple_out('Sh1', 0, 'save');
        simple_out('Sh2', 0, 'save');
        python(python_client_path, 'CALIBRATE_Z_RUN')
    end

    XY_positions = [-40, 0; 40, 0; 0, -40; 0, 40];
    Zlevels = [0, -50, -40, -30, -20, -10, 10, 20, 30, 40, 50];
    calibrate_correction_factor()
    calibrate_XY(XY_positions)
    %calibrate_Z(Zlevels)

    %python(python_client_path, 'CALIBRATE_RELEASE')
    disp('Done calibrating')
end

%python('holoclient_fem.py', 'CALIBRATE_CORRECTION_FACTOR', args)
% CALIBRATE_RUN = 3;
% CALIBRATE_BACKGROUND = 4;
% CALIBRATE_CIRCLE = 5;
% CALIBRATE_Z = 6;
% CALIBRATE_Z_RUN = 7;
% CALIBRATE_CORRECTION_FACTOR = 8;
% CALIBRATE_TIMING = 9; //sync between MES and holo, not implemented
% CALIBRATE_RELEASE = 10; //Releases the camera used by the calibration system






