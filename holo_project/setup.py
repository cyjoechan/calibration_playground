from distutils.core import setup
# from setuptools import setup

setup(
    name='holographics',
    version='0.9.0',
    packages=['holographics'],
    url=' ',
    license='GPL v3',
    author='Joe Donovan',
    author_email='joe@neuro.mpg.de',
    description='Holographics package',
    requires=[
        "Pillow (>=2.4.0)", "joblib (>=0.7.1)", "numpy (>=1.8.0)", "protobuf (>=2.5.0)", "scipy (>=0.13.0)", "svgwrite (>=1.1.5)", "pyglet (>=1.1)",
         "matplotlib (>=1.3.1)", "CairoSVG<=1.0.6" #pycairo 1.10.0  #cairosvg 1.0.4 to 1.0.6
    ],
    package_data={
    'holographics': ['LUT_info.ini', 'holo_config.cfg', 'compile_protoc.bat', 'holo_msg.proto', 'protoc.exe'],
    'deformation_correction_pattern': ['*.bmp'],
    },
    #need package_dir - see  https://docs.python.org/2/distutils/setupscript.html


    # data_files=[('deformation_correction_pattern', ['deformation_correction_pattern\CAL_LSH0700831_620nm.bmp'])],
)

#package_data works, but data_files doesn't -  turns out it want induvidual files, and fails silently on *
#other module dependcies
