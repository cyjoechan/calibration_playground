import random
from qt_holo.base_timingpower import BaseTimingPower
from qt_holo.ui_tp_randomorder_equaltiming import Ui_timingpowerform
from qt_holo.ROIset import ROIset


class TimingPower(BaseTimingPower, Ui_timingpowerform):
    """Random order, with equal durations
    """

    def __init__(self, graphicsView, outputview):
        super(BaseTimingPower, self).__init__()
        self.setupUi(self)
        self.generateButton.clicked.connect(self.generate)
        self.playButton.clicked.connect(self.play)
        self.mostrecent(False)
        self.graphicsView = graphicsView
        self.outputview = outputview

    def timingpower(self):
        newROIs = ROIset()
        indicies = range(len(self.ROIs))
        random.shuffle(indicies)  # random order
        for i, ROI in zip(indicies, self.ROIs):
            newROI = ROI.converttopowered()
            startttime = self.startDelaySpin.value() + i * (self.durationSpin.value() + self.isiSpin.value())
            newROI.seton(startttime, self.durationSpin.value(), power=self.powerSpin.value())

            newROIs.append(newROI)

        return newROIs
