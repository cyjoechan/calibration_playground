from qtpy import QtGui, QtWidgets, QtCore


class BaseROI(QtWidgets.QGraphicsObject):
    """
    Base for all ROIs, both for interaction or for outputviews
    """
    Type = QtWidgets.QGraphicsItem.UserType + 2

    def __init__(self, ZLevel):
        super(BaseROI, self).__init__()
        self.ZLevel = ZLevel

    @property
    def currentZLevel(self):
        return self.scene().parent().zlevel  # TODO self.scene().parent().window().currentZLevel

    @property
    def atCurrentZLevel(self):
        return self.ZLevel == self.currentZLevel

    # The methods below should be implemented in the child class:
    def paint(self):
        raise NotImplementedError

    def paintsvg(self):
        raise NotImplementedError

    def boundingRect(self):
        raise NotImplementedError

    def shape(self):
        raise NotImplementedError


class BaseInteractionROI(BaseROI):
    """
    Base class for all ROIs to be interacted with/edited
    """

    def __init__(self, *args, **kwargs):
        super(BaseInteractionROI, self).__init__(*args, **kwargs)
        self.setFlag(QtWidgets.QGraphicsItem.ItemIsMovable)
        self.setFlag(QtWidgets.QGraphicsItem.ItemSendsGeometryChanges)
        self.setAcceptHoverEvents(True)

    # def contextMenuEvent(self, event, *args, **kwargs):
    #     if not self.atCurrentZLevel:
    #         m = QtGui.QMenu()
    #         m.addAction("Jump to Z", self.jumptoZ)
    #         m.addAction("Move to current Z", self.movetocurrentZ)
    #         m.exec_(event.screenPos())
    #         return
    #     m = QtGui.QMenu()
    #     m.addAction("Delete", self.deleteLater)
    #     # m.addAction("Split ROI", self.split)
    #     m.exec_(event.screenPos())

    def jumptoZ(self):
        self.scene().parent().setzlevel(self.ZLevel)

    def movetocurrentZ(self):
        self.ZLevel = self.currentZLevel
        self.update()

    def hoverEnterEvent(self, *args, **kwargs):
        if self.atCurrentZLevel:
            self.setCursor(QtCore.Qt.OpenHandCursor)

    def hoverLeaveEvent(self, event):
        if self.atCurrentZLevel:
            self.setCursor(QtCore.Qt.ArrowCursor)


class BasePoweredROI(BaseROI):
    """
    Base class for output ROIs, with timing and powerinfo
    """
    Type = QtWidgets.QGraphicsItem.UserType + 3

    def __init__(self, Zlevel):
        super(BasePoweredROI, self).__init__(Zlevel)
        self.timingpower = None
        self.times = [0]
        self.powers = [0]

    # TODO could validate any changes to times and power, and check types

    @property
    def maxpower(self):
        return max(self.powers)

    @property
    def duration(self):
        return max(self.times)

    def powerat(self, atime):
        power = -1
        for t, p in zip(self.times, self.powers):
            if atime < t:
                break
            power = p
        # assert power != -1 #TODO assert and then handle 'power off' exception?
        return power

    def seton(self, starttime, duration, power):
        self.times.append(starttime)
        self.powers.append(power)
        self.times.append(starttime + duration)
        self.powers.append(0)
        # could sort based on timing orders? or insert smartly

# TODO need to have a powered fromnormal roi method??
