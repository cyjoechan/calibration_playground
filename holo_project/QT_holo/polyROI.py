from __future__ import division
import warnings
import numpy as np
from matplotlib.path import Path
from shapely.geometry import Polygon
from PIL import Image, ImageDraw
from qtpy import QtGui, QtCore, QtWidgets, QtSvg
from qt_holo.splitDialog import SplitDialog
from qt_holo import baseROI


class PolyNode(QtWidgets.QGraphicsItem):
    Type = QtWidgets.QGraphicsItem.UserType + 1

    def __init__(self, parent, pos):
        super(PolyNode, self).__init__()
        self.setParentItem(parent)
        self.setFlag(QtWidgets.QGraphicsItem.ItemIsMovable)
        self.setFlag(QtWidgets.QGraphicsItem.ItemSendsGeometryChanges)
        self.setFlag(QtWidgets.QGraphicsItem.ItemIgnoresTransformations)
        self.setAcceptHoverEvents(True)
        self.setPos(pos)
        self.halfSize = 6
        self.setZValue(5)

    def boundingRect(self):
        adjust = 0.0
        return QtCore.QRectF(-self.halfSize - adjust, -self.halfSize - adjust, 2 * (self.halfSize + adjust),
                             2 * (self.halfSize + adjust))

    @property
    def getCoords(self):
        return (-self.halfSize, -self.halfSize, self.halfSize * 2, self.halfSize * 2)

    def shape(self):
        path = QtGui.QPainterPath()
        path.addEllipse(*self.getCoords)
        return path

    def paint(self, painter, option, widget):
        gradient = QtGui.QRadialGradient(-3, -3, 10)
        if option.state & QtGui.QStyle.State_Sunken:
            gradient.setCenter(3, 3)
            gradient.setFocalPoint(3, 3)
            gradient.setColorAt(1, QtGui.QColor(QtCore.Qt.yellow).lighter(120))
            gradient.setColorAt(0, QtGui.QColor(QtCore.Qt.darkYellow).lighter(120))
            painter.setBrush(QtGui.QBrush(gradient))
            pen = QtGui.QPen(QtGui.QColor('white'))
            pen.setCosmetic(True)
            pen.setWidthF(1)
            painter.setPen(pen)
            # painter.setPen(QtGui.QPen(QtCore.Qt.black, 0))
        else:
            light_red = QtGui.QColor('Blue')
            # light_red.setAlpha(200)
            painter.setBrush(QtGui.QBrush(light_red))
            pen = QtGui.QPen(QtGui.QColor('white'))
            pen.setCosmetic(True)
            pen.setWidthF(1)
            painter.setPen(pen)

        painter.drawEllipse(QtCore.QRectF(*self.getCoords))  # QRectF, since the overloaded drawEllipse takes ints...

    def mousePressEvent(self, event):
        self.update()
        super(PolyNode, self).mousePressEvent(event)

    def mouseMoveEvent(self, QGraphicsSceneMouseEvent):
        self.parentItem().prepareGeometryChange()
        self.parentItem().update()
        super(PolyNode, self).mouseMoveEvent(QGraphicsSceneMouseEvent)

    def mouseReleaseEvent(self, event):
        self.update()
        super(PolyNode, self).mouseReleaseEvent(event)

    def contextMenuEvent(self, event, *args, **kwargs):
        m = QtGui.QMenu()
        m.addAction("Delete", self.deleteLater)
        m.exec_(event.screenPos())

    def deleteLater(self):
        self.parentObject().prepareGeometryChange()
        self.setParentItem(None)

    # def mouseDoubleClickEvent(self, QGraphicsSceneMouseEvent):
    #     QGraphicsSceneMouseEvent.ignore()

    def hoverEnterEvent(self, *args, **kwargs):
        self.setCursor(QtCore.Qt.SizeAllCursor)


class PolyGeometry(object):
    """
    Base class for shared polygon geometry
    """

    def __init__(self, *args, **kwargs):
        super(PolyGeometry, self).__init__(*args, **kwargs)

    @property
    def nodes(self):
        return [c for c in self.childItems() if isinstance(c, PolyNode)]

    @property
    def nodepositions(self):
        return [c.scenePos() for c in self.nodes]

    @property
    def nodepositionspy(self):
        nodes = [c.scenePos() for c in self.nodes]
        return [(float(n.x()), float(n.y())) for n in nodes]

    @property
    def nodepositionsnp(self):
        return np.asarray(self.nodepositionspy)

    def __len__(self):
        return len(self.nodes)

    def boundingRect(self):
        return self.poly.boundingRect()

    def shape(self):
        path = QtGui.QPainterPath()
        path.addPolygon(self.poly)
        return path

    @property
    def poly(self):
        return QtGui.QPolygonF([n.pos() for n in self.nodes])

    @property
    def polyScene(self):  # in scene coords
        return QtGui.QPolygonF([n.scenePos() for n in self.nodes])

    def addNode(self, pos):
        # print self.mapToScene(pos)
        self.prepareGeometryChange()
        PolyNode(self, self.mapToScene(pos))

    @property
    def centroid(self):
        centroid = Polygon(self.nodepositionspy).centroid
        return centroid.x, centroid.y


class PolyROI(PolyGeometry, baseROI.BaseInteractionROI):
    Type = QtWidgets.QGraphicsItem.UserType + 3

    def changedZ(self, zlevel=None):
        if self.atCurrentZLevel:
            self.setZValue(3)
        else:
            self.setZValue(2)

    def split(self):
        accepted, polys = SplitDialog.run(self.nodes, self.scene().views()[0])  # 2nd arg is drawwidget/graphicsview
        if accepted:
            self.deleteLater()
            for poly in polys:
                newpoly = PolyROI(self.ZLevel)
                [newpoly.addNode(self.mapToScene(QtCore.QPointF(p[0], p[1]))) for p in poly]
                self.scene().addItem(newpoly)

    def paint(self, painter, option, widget):
        # print painter, painter.device(), painter.paintEngine()
        # print option
        if isinstance(painter.device(), QtSvg.QSvgGenerator):
            raise Exception("This paint shouldn't get called by SVG!")

        else:  # normal drawing:
            if self.atCurrentZLevel:
                [n.setVisible(True) for n in self.nodes]
                color = QtGui.QColor('Blue')
                color.setAlpha(55)
                brush = QtGui.QBrush(color)
                pen = QtGui.QPen(QtGui.QColor('White'))
                pen.setCosmetic(True)
                pen.setWidthF(1)
                painter.setBrush(brush)
                painter.setPen(pen)
                painter.drawPolygon(self.poly)
                self.setFlag(QtGui.QGraphicsItem.ItemIsMovable)
            else:
                [n.setVisible(False) for n in self.nodes]
                color = QtGui.QColor('Blue')
                color.setAlpha(32)
                brush = QtGui.QBrush(color)
                colorpen = QtGui.QColor('Blue')
                colorpen.setAlpha(135)
                pen = QtGui.QPen(colorpen)
                pen.setCosmetic(True)
                pen.setStyle(QtCore.Qt.DashDotLine)
                pen.setWidthF(1)
                painter.setBrush(brush)
                painter.setPen(pen)
                painter.drawPolygon(self.poly)
                self.setFlag(QtGui.QGraphicsItem.ItemIsMovable, False)

    def contextMenuEvent(self, event, *args, **kwargs):
        if not self.atCurrentZLevel:
            m = QtGui.QMenu()
            m.addAction("Jump to Z", self.jumptoZ)
            m.addAction("Move to current Z", self.movetocurrentZ)
            m.exec_(event.screenPos())
            return
        m = QtGui.QMenu()
        m.addAction("Delete", self.deleteLater)
        m.addAction("Copy", self.setascopytarget)
        # m.addAction("Split ROI", self.split)
        m.exec_(event.screenPos())

    def setascopytarget(self):
        self.scene().parent().setcopytarget(self)

    def paintsvg(self, painter):
        color = QtGui.QColor(255, 255, 255)
        brush = QtGui.QBrush(color)
        painter.setBrush(brush)

        pen = QtGui.QPen()
        pen.setStyle(QtCore.Qt.NoPen)
        painter.setPen(pen)
        painter.drawPolygon(self.polyScene)

    def mousePressEvent(self, event):
        super(PolyROI, self).mousePressEvent(event)
        if self.atCurrentZLevel:
            self.setCursor(QtCore.Qt.ClosedHandCursor)
        else:
            event.ignore()

    def mouseReleaseEvent(self, event):
        super(PolyROI, self).mouseReleaseEvent(event)
        if self.atCurrentZLevel:
            self.setCursor(QtCore.Qt.OpenHandCursor)
        else:
            event.ignore()

            # def mouseMoveEvent(self, event):
            # super(PolyROI, self).mouseMoveEvent(event)
            # print event.modifiers() == QtCore.Qt.ControlModifier

    def converttopowered(self):
        return PoweredPoly(self.ZLevel, self.nodepositions)

    def todict(self):
        return {'z': self.ZLevel, 'xy': self.nodepositionspy}

    @classmethod
    def fromdict(cls, zxydict):
        c = cls(None)
        c.ZLevel = zxydict['z']
        [c.addNode(QtCore.QPointF(*xy)) for xy in zxydict['xy']]
        return c

    def getmask(self, shape, sceneRect):
        """
        Returns mask of ROI shape
        """
        # TODO if outside of the image, should throw an exception?
        h, w = shape
        xys = np.asarray(self.nodepositionspy)
        xys -= sceneRect[:2]  # shift origins to match
        trans = np.asarray(([w / sceneRect[2], 0], [0, h / sceneRect[3]]))
        xys = np.dot(xys, trans)

        # TODO checking points is slow, try shapely, or better a more efficient algo
        # checkout pyqtgraph, ROI getArraySlice
        path = Path(xys)
        # path = Polygon(xys)
        y, x = np.mgrid[:h, :w]
        points = np.transpose((x.ravel(), y.ravel()))
        mask = path.contains_points(points)
        if sum(mask) == 0:
            warnings.warn("ROI mask empty!")
        mx = mask.reshape(h, w).astype(np.bool)
        return mx

    def getmaskidx(self, shape, sceneRect):
        mask = self.getmask(shape, sceneRect)
        return mask.nonzero()

    def masked(self, stack, sceneRect):
        mask = self.getmask(stack.shape[1:-1], sceneRect)
        masked = np.ma.MaskedArray(stack[..., self.ZLevel], np.repeat(mask[None, ...], stack.shape[0], 0))
        # r = mask * stack[..., self.ZLevel] #in correct dim
        return masked

    def timeseries(self, stack, sceneRect):
        x1, x2 = self.getmaskidx(stack.shape[1:-1], sceneRect)
        return np.percentile(stack[:, x1, x2, self.ZLevel], 80, 1)
        # return np.mean(stack[:, x1, x2, self.ZLevel], 1)

    def __repr__(self):
        return "PolyROI: z: %s xy: %s" % (self.ZLevel, repr(self.nodepositionspy))


class PoweredPoly(PolyGeometry, baseROI.BasePoweredROI):
    def __init__(self, Zlevel, nodepositions):
        super(PoweredPoly, self).__init__(Zlevel)
        [self.addNode(pos) for pos in nodepositions]
        [n.setVisible(False) for n in self.nodes]

    @property
    def currenttime(self):
        return self.scene().parent().time

    def paint(self, painter, option, widget):
        # print painter, painter.device(), painter.paintEngine()
        # print option
        color = None
        if self.atCurrentZLevel:
            if int(self.powerat(self.currenttime)) == 0 and self.scene().parent().showoutoftime():
                color = QtGui.QColor(0, 0, 255, 50)
            else:
                powernormed = int(255 * self.powerat(self.currenttime) / self.scene().parent().maxrawpower)
                color = QtGui.QColor(255, 255, 255, powernormed)

        elif self.scene().parent().showoutofZ():
            if int(self.powerat(self.currenttime)) == 0:
                color = QtGui.QColor(255, 0, 0, 50)
            else:
                color = QtGui.QColor(0, 255, 0, 50)

        if color:
            brush = QtGui.QBrush(color)
            painter.setBrush(brush)

            pen = QtGui.QPen()
            pen.setStyle(QtCore.Qt.NoPen)
            painter.setPen(pen)
            painter.drawPolygon(self.poly)

    def paintsvg(self, painter, time, ZLevel, option=None, widget=0):
        if self.ZLevel == ZLevel:
            powernormed = int(self.powerat(time))  # /self.scene().parent().maxrawpower)
            if powernormed <= 0:
                return  # TODO is this the right place for this logic?
            color = QtGui.QColor(255, 255, 255, powernormed)
            brush = QtGui.QBrush(color)
            painter.setBrush(brush)

            pen = QtGui.QPen()
            pen.setStyle(QtCore.Qt.NoPen)
            painter.setPen(pen)
            painter.drawPolygon(self.poly)


if __name__ == '__main__':
    print(PoweredPoly.__mro__)
    pass

    # TODO scale invariant/semi-invariant node size across zooms

    # TODO convex hull of the polyROI node points?
    # from scipy.spatial import ConvexHull
    # ConvexHull(([1,1],[20,2], [3,4])).points
    # n > 3, needs a try, catch prolly
