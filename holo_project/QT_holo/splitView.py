from qtpy import QtGui, QtCore
from qt_holo.TIFView import TIFView


class SplitView(TIFView):
    def __init__(self, parent):
        super(SplitView, self).__init__(parent)

    def clear(self):
        [item.setParentItem(None) for item in self.scene().items() if isinstance(item, QtGui.QGraphicsPolygonItem)
         or isinstance(item, QtGui.QGraphicsEllipseItem)]
