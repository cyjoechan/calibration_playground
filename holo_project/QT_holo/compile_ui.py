from PyQt5 import uic
# from qtpy import uic
import os
import sys

""" what works: 
pyuic5 ui_holo_main.ui -o ui_holo_main.py
"""


def compileuis(dir='.'):
    filelist = [p for p in os.listdir(dir) if p.endswith('.ui')]
    print(filelist)
    for uifile in filelist:
        # uifile = os.path.join(os.getcwd(), uifile)
        targetpath = os.path.splitext(uifile)[0] + ".py"
        # print os.path.abspath(targetpath)
        with open(targetpath, 'w') as pyfile:
            # uic.compileUi(uifile, pyfile)
            uic.compileUi(uifile, pyfile)
            # print(uic.loadUi(uifile))


if __name__ == "__main__":
    compileuis()
