from qtpy import QtGui, QtCore, QtWidgets
import itertools


class BaseTimingPower(QtWidgets.QWidget, QtWidgets.QListWidgetItem):
    def __init__(self, graphicsView, outputview):
        super(BaseTimingPower, self).__init__()
        self.outputview = outputview
        self.grpahicsView = graphicsView
        self.pushClose.clicked.connect(self.deleteLater)

    @property
    def ROIs(self):
        return self.graphicsView.ROIs

    def mostrecent(self, bool=True):
        """Sets a visual indicator, of whether this was the most recently generated timingpower
        """
        if bool:
            self.window().pattern_valid(False)  # to reset others
        self.mostRecentLabel.setVisible(bool)
        if bool:
            self.mostRecentLabel.lower()
            self.window().pattern_valid(True)

    def play(self):
        self.window().holoclient.play()

    @staticmethod
    def keytimes(newROIs):
        times = list(set(itertools.chain(*[ROI.deltatimes for ROI in newROIs])))
        times.sort()
        return times

    def generate(self):
        if len(self.ROIs) == 0:
            qmsg = QtGui.QMessageBox.warning(self, "Warning", "No ROI targets defined, can't generate!")
            return
        newROIs = self.timingpower()
        self.outputview.setNewROIS(newROIs)
        self.mostrecent()

    def timingpower(self):
        """
        Overwrite this in new TP classes
        Generates timing and power info, adds to ROIset
        returns ROIset
        """
        raise NotImplementedError
