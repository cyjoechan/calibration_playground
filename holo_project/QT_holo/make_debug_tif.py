import numpy as np
from skimage import io


def makeTIF(width=400, height=250, zsize=20):
    # return np.zeros((zsize, width, height, 1), 'uint8')
    return np.zeros((width, height, zsize), 'uint16')
    # return np.zeros((height, width, 1, zsize), 'uint8')


def saveTIF(array, filepath='test.tif'):
    """
    array is assumed to be in width, height, zorder
    simpleitk takes z, height, width
    """
    # stack = np.rollaxis(io.imread(filepath, plugin='simpleitk'), 0, 3)
    if array.ndim == 3:
        array = array[..., None]

    # array = np.rollaxis(array, 1, 0)
    array = np.swapaxes(array, 2, 0)
    io.imsave(filepath, array, plugin='simpleitk')


if __name__ == "__main__":
    array = makeTIF()
    array[0, 0, 0] = 255

    saveTIF(array)

    # print array.shape
    # array = np.rollaxis(array, 0, 3)
    # print array.shape
    # print np.rollaxis(array, 0, 1).shape
    # print
