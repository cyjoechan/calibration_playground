import sys
import os

if not os.path.abspath("..") in sys.path:
    sys.path.append(os.path.abspath(".."))

if __name__ == "__main__" and __package__ is None:
    __package__ = "holographics"


    # print 'pkg', __package__
    # print __name__
