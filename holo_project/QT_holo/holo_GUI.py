from __future__ import division
import os

# os.environ.setdefault("QT_API", 'pyside')
# os.environ.setdefault("QT_API", 'PyQt5')

from qtpy import QtGui, QtCore, QtWidgets

# QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling, True)
# see also http://doc.qt.io/qt-5/highdpi.html

import sys
import os
import numpy as np
from skimage import io
import imp

from qt_holo import ui_holo_main
from qt_holo.metadata_parse import FemtonicsTifMeta
from qt_holo.paintSVG import generateSVG

import qt_holo.add_package_path
from holographics.holoclient_lib import Holoclient
from holographics.frame import Frame

# TODO fix this sister module import mess
# apparently it's not good/easy to use relative imports within package, but is moving the main part of each file outside the project any better?
# maybe best is 3 indepent packages for each, with a add_path module that adds .. if it's not already on the right path
# if __name__ == "__main__" and __package__ is None:
#     __package__ = "holographics.qt_holo"
# print __package__, __name__
# from .. import holographics.holoclient

defaulttifpath = "C:/stack_examples"  # TODO move to config or auto determine
defaulttppath = "C:/holo_project/qt_holo"


class HoloMain(QtWidgets.QMainWindow, ui_holo_main.Ui_MainWindow):
    def __init__(self):
        super(HoloMain, self).__init__()
        self.setupUi(self)

        self.createActions()
        self.createMenus()

        self.TIF = None
        self.TIFmeta = None
        self.TIFfilename = None
        self.setWindowTitle("Holo GUI")
        # self.move(QtGui.QDesktopWidget().screenGeometry(0).left()+100,50)

        self.holoclient = Holoclient()
        print("Done connecting to server")

        self.graphicsView.add_slider(self.horizontalSlider)
        self.graphicsView.levelChanged.connect(self.updateZLabelClosure(self.label_framenum))
        self.graphicsView.levelChanged.connect(self.updateZLevelLabelClosure(self.label_Zlevel))
        self.objZSlider.valueChanged.connect(self.updateZlevel)

        self.outputView.add_slider(self.outputZLevelSlider)
        self.outputView.levelChanged.connect(self.updateZLabelClosure(self.outputlabel_framenum))
        self.outputView.levelChanged.connect(self.updateZLevelLabelClosure(self.outputlabel_Zlevel))

        self.outputTimeSlider.valueChanged.connect(self.outputView.updateOutputTime)

        self.generateButton.clicked.connect(self.generate)
        self.playButton.clicked.connect(self.holoclient.play)
        self.clearROIsButton.clicked.connect(self.graphicsView.clearROIs)
        self.showOutOfTime.stateChanged.connect(self.outputView.doupdate)
        self.showOutOfZ.stateChanged.connect(self.outputView.doupdate)
        self.ouputPlayButton.clicked.connect(self.outputView.play)
        # self.loadSVGsButton.clicked.connect(self.outputView.overlaySVGs)
        self.graphicsView.mouseMoved.connect(self.updatePosLabels)

        self.doTest.clicked.connect(self.loadtest)  # TODO remove test button
        self.doTest.setFlat(True)

        # sets up timing and power layout - going thru desinger adds an extra widget into the hierarchy
        self.addtimingpower.clicked.connect(self.addtimingpowerwidget)
        self.scrollAreaWidgetContents.setLayout(QtWidgets.QVBoxLayout(self.scrollArea))
        self.scrollAreaWidgetContents.setMinimumSize(600, 400)
        self.scrollAreaWidgetContents.setSizePolicy(QtWidgets.QSizePolicy.MinimumExpanding,
                                                    QtWidgets.QSizePolicy.MinimumExpanding)
        self.scrollAreaWidgetContents.layout().setSizeConstraint(QtWidgets.QLayout.SetMinAndMaxSize)
        self.addtimingpowerwidget("randomorder_equaltiming.tp.py")

        self.pattern_valid(False)

        self.updateActions()
        self.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)

        self.currentZLevel = None
        self.currentoutputZLevel = None

    def addtimingpowerwidget(self, filepath=None):
        if not filepath:
            filepath = QtWidgets.QFileDialog.getOpenFileName(self, "Open File",
                                                         defaulttppath, "TP (*.tp.py)")[0]
        if filepath:
            newtimingpower = imp.load_source('test', filepath)
            tp = newtimingpower.TimingPower(self.graphicsView, self.outputView)
            self.scrollAreaWidgetContents.layout().addWidget(tp)

    def loadtest(self):
        self.open('C:/stack_examples/Zstachpagfp_pmtUG.tif')
        from .polyROI import PolyROI
        p = PolyROI(0)
        self.graphicsView.scene().addItem(p)
        p.addNode(QtCore.QPointF(-40, -40))
        p.addNode(QtCore.QPointF(20, 30))
        p.addNode(QtCore.QPointF(30, 0))

        p = PolyROI(4)
        self.graphicsView.scene().addItem(p)
        p.addNode(QtCore.QPointF(-30, 20))
        p.addNode(QtCore.QPointF(-30, 0))
        p.addNode(QtCore.QPointF(-5, 0))
        self.updateZlevel(0)

    def generate(self):
        svg = generateSVG(self.graphicsView.ROIs, self.TIFfilename)
        self.holoclient.generate([Frame(svg, Zlevel=0, frame_num=0, duration=4.0)],
                                 wavelength=self.window().wavelengthSpinBox.value(),
                                 correction_factor=self.window().SLMcorrSpinBox.value())

    def open(self, filepath=None, normalize=True):
        if not filepath:
            filepath = QtWidgets.QFileDialog.getOpenFileName(self, "Open File", defaulttifpath, "TIFs (*.tif; *.tiff)")[0]
            # pyside returns a tuple
        if filepath:
            filepath = str(os.path.normpath(filepath))  # imread can't handle unicode filepaths
            singlestackformat = False
            try:
                read = io.imread(filepath, plugin='simpleitk')
                if read.ndim == 3:
                    stack = np.rollaxis(read, 0, 3)
                else:
                    singlestackformat = True
                    stack = read[:, :, None]
                del read

                if normalize:
                    # stack = stack.astype('float16')
                    stack_min = np.percentile(stack, .001, interpolation='nearest')
                    stack[stack < stack_min] = stack_min
                    stack -= stack_min

                    stack_max = max(1,
                                    np.percentile(stack, 99.9, interpolation='nearest'))  # max in case percentile is 0
                    stack[stack > stack_max] = stack_max

                    stack = np.right_shift(stack * (np.iinfo(stack.dtype).max / stack_max).astype(stack.dtype), 8)
                    stack = stack.astype('uint8')

                self.TIF = stack
                self.TIFfilename = os.path.split(filepath)[1]
            except RuntimeError:  # thrown by ITK engine
                QtGui.QMessageBox.information(self, "Holo GUI", "Cannot load %s." % filepath)
            else:
                self.TIFmeta = None
                try:  # to autoload metadata
                    filepath, filename = os.path.split(filepath)
                    splitted = filename.split('_')
                    for ending in [".tif_metadata.txt", "_metadata.txt",
                                   ".metadata.txt"]:  # two different possible filenames, depending on how it was exported from MES
                        metadata_guess = os.path.join(filepath, '_'.join(splitted[:-1]) + ending)
                        if os.path.exists(metadata_guess):
                            self.TIFmeta = FemtonicsTifMeta(metadata_guess, singlestackformat)
                            self.TIFmeta.check_valid(self.TIF)
                    if self.TIFmeta is None:
                        raise AssertionError
                except (IOError, AssertionError, KeyError, IndexError) as e:
                    self.TIFmeta = None
                    metadata_path = QtWidgets.QFileDialog.getOpenFileName(self, "Open Metadata File", defaulttifpath,
                                                                      "Metadata files (*.txt)")[0]
                    if metadata_path:
                        try:
                            self.TIFmeta = FemtonicsTifMeta(metadata_path, singlestackformat)
                            self.TIFmeta.check_valid(self.TIF)
                        except (AssertionError, KeyError) as e:
                            QtGui.QMessageBox.information(self, "Holo GUI", "Incorrect metadata file %s." % filepath)

                self.horizontalSlider.setMaximum(self.TIF.shape[2] - 1)
                self.objZSlider.setMaximum(self.TIF.shape[2] - 1)
                self.outputZLevelSlider.setMaximum(self.TIF.shape[2] - 1)

                self.graphicsView.clearROIs()  # user might not want to, but want about ROIs outside the view??

                if self.TIFmeta:
                    self.graphicsView.newTIF(self.TIF, self.TIFmeta.image_rect)
                else:
                    self.graphicsView.newTIF(self.TIF, (0, 0, self.TIF.shape[1], self.TIF.shape[0]))
                self.outputView.fromTIFView(self.graphicsView)

                self.updateActions()
                # self.updateZlevel(0)
                # self.updateOutputZlevel(0)

    def updateZlevel(self, slice_num):
        self.currentZLevel = slice_num
        if self.horizontalSlider.value() != slice_num:
            self.horizontalSlider.setValue(slice_num)

    def updateZLabelClosure(self, label):
        def updatezlabel(level):
            label.setText(str(level))

        return updatezlabel

    def updateZLevelLabelClosure(self, label):
        def updatezlabel(level):
            if self.TIFmeta:
                label.setText(str(self.TIFmeta.Z(level)) + " um")
            else:
                label.setText("No metadata loaded")

        return updatezlabel

    #         # reorder qt Zlevels, so ROIs at current level are on top
    #         for roi in view.ROIs:
    #             if roi.atCurrentZLevel:
    #                 roi.setZValue(10)
    #             else:
    #                 roi.setZValue(5)

    @property
    def objZlevel(self):
        return self.TIFmeta.Z(self.objZSlider.value())

    @property
    def TIFvalid(self):
        return self.TIF is not None

    def zoomIn(self):
        self.graphicsView.scaleView(1.25)

    def zoomOut(self):
        self.graphicsView.scaleView(1 / 1.25)

    def normalSize(self):
        self.graphicsView.scaleView(0.0)

    def createActions(self):
        self.openAct = QtWidgets.QAction("&Open...", self, shortcut="Ctrl+O", triggered=self.open)

        self.exitAct = QtWidgets.QAction("E&xit", self, shortcut="Ctrl+Q", triggered=self.close)

        self.zoomInAct = QtWidgets.QAction("Zoom &In (25%)", self,
                                           shortcut="Ctrl++", enabled=False, triggered=self.zoomIn)
        self.zoomInAct.setShortcuts(['Ctrl++', 'Ctrl+='])
        self.zoomOutAct = QtWidgets.QAction("Zoom &Out (25%)", self,
                                            shortcut="Ctrl+-", enabled=False, triggered=self.zoomOut)

        self.normalSizeAct = QtWidgets.QAction("&Normal Size", self,
                                               shortcut="Ctrl+S", enabled=False, triggered=self.normalSize)

    def createMenus(self):
        self.fileMenu = QtWidgets.QMenu("&File", self)
        self.fileMenu.addAction(self.openAct)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.exitAct)

        self.viewMenu = QtWidgets.QMenu("&View", self)
        self.viewMenu.addAction(self.zoomInAct)
        self.viewMenu.addAction(self.zoomOutAct)
        self.viewMenu.addAction(self.normalSizeAct)

        self.menuBar().addMenu(self.fileMenu)
        self.menuBar().addMenu(self.viewMenu)

    def updateActions(self):
        self.zoomInAct.setEnabled(self.TIFvalid)
        self.zoomOutAct.setEnabled(self.TIFvalid)
        self.normalSizeAct.setEnabled(self.TIFvalid)
        self.horizontalSlider.setEnabled(self.TIFvalid)
        self.objZSlider.setEnabled(self.TIFvalid)

    def updatePosLabels(self, pos_scene):
        pos = "(%d, %d)" % (int(pos_scene.x()), int(pos_scene.y()))
        if self.TIFmeta:
            pos += " um"
        self.label_xy.setText(pos)

    def pattern_valid(self, bool):
        """Resets visual flags, to indicate that the previously generated pattern is no longer valid"""
        if bool:
            self.mostRecentLabel.setVisible(False)
        else:
            self.mostRecentLabel.setVisible(True)
            for i in range(self.scrollAreaWidgetContents.layout().count()):
                self.scrollAreaWidgetContents.layout().itemAt(i).widget().mostrecent(False)


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    holoGUI = HoloMain()
    holoGUI.show()
    app.exec_()
    # del holoGUI #without this there's some access violation on exit, since it doesn't get cleaned up right

# TODO fire an update signal to all TP widgets, on a tab switch to timing power, maybe together with ROI changed flag,
# so widgets can include ROIs in options (ie checkbox series)

# TODO improve how holoclient works, add timesout

# TODO text when currently editing/adding nodes
# TODO zoom bar

# TODO connect arrow keys to movement from not just movementbar
# TODO panning - http://www.qtcentre.org/wiki/index.php?title=QGraphicsView:_Smooth_Panning_and_Zooming
