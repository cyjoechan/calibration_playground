import time


class FemtonicsTifMeta(object):
    """
    Parses metadata from Femtonics .txt tif associated files
    """

    def __init__(self, filepath, singlestackformat=False):
        with open(filepath, 'rb') as metatxt:
            data = metatxt.readlines()
            self.data_dict = {}
            for d in data:
                if len(d.split(':')) == 2:
                    self.data_dict[d.split(':')[0].strip()] = d.split(':')[1]
                elif len(d.split(':')) > 2:
                    if d.split(':')[0].strip() == 'FileName':
                        self.data_dict['FileName'] = d[d.find(':') + 1:].strip()
                    elif d.split(':')[0].strip() == 'MeasurementDate':
                        self.data_dict['MeasurementDate'] = time.strptime(d[d.find(':') + 1:d.find(',')].strip(),
                                                                          "%Y.%m.%d. %X")
                    else:
                        pass
                        # print "Failed to parse %s" % d
                else:
                    pass
                    # print "Failed to parse %s" % d

            self.convert_types()
            self.singlestackformat = singlestackformat
            if self.singlestackformat:
                self.fixsinglestack()

    def fixsinglestack(self):
        """
        Fixes the metadata from femtonics, - if you export a single tif, the format is different.
        """
        self.data_dict['D3Size'] = 1
        self.data_dict['D3Step'] = 2.5
        self.data_dict['D3Origin'] = self.ZlevelArm

    def check_valid(self, tif):
        assert tif.shape[2] == self.D3Size
        assert tif.shape[0] == self.Height
        assert tif.shape[1] == self.Width

    def convert_types(self):
        for key, value in self.data_dict.iteritems():
            if key in ["MeasurementDate", "FileName"]:
                continue
            try:
                self.data_dict[key] = int(self.data_dict[key])
            except ValueError:
                try:
                    self.data_dict[key] = float(self.data_dict[key])
                except ValueError:
                    pass

    def __str__(self):
        return '\n'.join([str(i) + " " + str(j).strip() + " " + str(type(j)) for i, j in self.data_dict.items()])
        # TODO could print in order

    def __getattr__(self, item):
        return self.data_dict[item]

    # @property
    def Z(self, slice):
        """Get Z from origin of slice#, using python/zero based index"""
        # print slice, self.D3Size
        assert 0 <= slice < self.D3Size
        return (self.D3Origin - self.ZlevelArm) + slice * self.D3Step

    @property
    def width_um(self):
        return self.WidthStep * self.Width

    @property
    def height_um(self):
        return self.HeightStep * self.Height

    @property
    def image_rect(self):
        """image pos, image size for QT"""
        return (self.WidthOrigin, -(self.height_um + self.HeightOrigin), self.width_um, self.height_um)

    def pos_to_um(self, pos):
        x = self.WidthOrigin + pos[0] * self.WidthStep
        y = -self.HeightOrigin + pos[
                                     1] * self.HeightStep  # QT/tif coords are top left origin, femtonics is bottom left (math)
        return (x, y)

    @classmethod
    def fromtifpath(cls, tifpath):
        return cls(tifpath.split('_')[0] + "metadata.txt")  # TODO test

    @property
    def shape(self):
        return self.Height, self.Width

        # @property
        # def minZ(self):
        #     return self.D3Origin, self.D3Size, self.D3Step


if __name__ == "__main__":
    path = r'stack_example\testforjoe.tif_metadata.txt'
    path = r'stack_example\zholo.tif_metadata.txt'

    tifmeta = FemtonicsTifMeta(path)
    print(tifmeta)
    print(tifmeta.Z(0))
    print(tifmeta.Z(20))
    print(tifmeta.WidthStep * tifmeta.Width)
    print(tifmeta.image_rect)
