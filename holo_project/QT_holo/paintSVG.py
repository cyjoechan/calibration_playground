from qtpy import QtGui, QtCore, QtSvg
from qt_holo import add_package_path
from holographics.svg_util import add_background


def generateSVG(ROIs, TIFname=None, *args, **kwargs):
    """
    Takes in a list of ROIs, and paints them into a buffer in SVG format
    """

    svggen = QtSvg.QSvgGenerator()
    svggen.setFileName('unused.svg')
    output = QtCore.QBuffer()
    svggen.setOutputDevice(output)
    svggen.setSize(QtCore.QSize(800, 800))
    svggen.setViewBox(QtCore.QRect(-400, -400, 800, 800))

    svggen.setTitle("Joe's Holo SVG Generator")
    if TIFname is None:
        TIFname = 'TIFname not set'
    svggen.setDescription("Exported from qt_holo, TIFname: " + TIFname)
    painter = QtGui.QPainter()
    painter.begin(svggen)

    # paint some background
    pen = QtGui.QPen()
    pen.setStyle(QtCore.Qt.NoPen)
    painter.setPen(pen)
    color = QtGui.QColor(0, 0, 0)
    brush = QtGui.QBrush(color)
    painter.setBrush(brush)
    # painter.drawRect(-400, -400, 800, 800)

    for ROI in ROIs:
        ROI.paintsvg(painter, *args, **kwargs)

    painter.end()
    return add_background(output.data())
