from __future__ import division
import math
from qtpy import QtGui, QtWidgets, QtCore


class TIFView(QtWidgets.QGraphicsView):
    """
    Base class for viewing TIFs, used by drawwidget and outputview
    """

    def __init__(self, parent):
        super(TIFView, self).__init__(parent)

        self.setBackgroundRole(QtGui.QPalette.Dark)
        self.setScene(QtWidgets.QGraphicsScene(self))

        self.TIFpixmap = None
        self.minZoom = 2.0
        self.maxZoom = 100.0

        self.setRenderHint(QtGui.QPainter.Antialiasing)
        self.setTransformationAnchor(QtWidgets.QGraphicsView.AnchorUnderMouse)
        # self.setResizeAnchor(QtGui.QGraphicsView.AnchorViewCenter)

        self.TIFpixmap = self.scene().addPixmap(QtGui.QPixmap())
        self.TIFpixmap.setTransformationMode(QtCore.Qt.SmoothTransformation)

        # self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        # self.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)

        # self.setViewportUpdateMode(QtGui.QGraphicsView.BoundingRectViewportUpdate)
        # redraws only whats in the viewport, see see http://qt-project.org/doc/qt-4.8/qgraphicsview.html#ViewportUpdateMode-enum

    def newTIF(self, sceneRect, TIFshape):  # TODO refactor argument order
        # print sceneRect, TIFshape
        self.scene().setSceneRect(*sceneRect)  # otherwise when switching to a new tif scene is off
        self.setSceneRect(self.scene().sceneRect())
        self.centerOn(sceneRect[0] + sceneRect[2] * .5, sceneRect[1] + sceneRect[3] * .5)
        self.update_minZoom()
        self.TIFpixmap.setPos(sceneRect[0], sceneRect[1])
        self.TIFpixmap.setScale(min(sceneRect[2] / TIFshape[1], sceneRect[3] / TIFshape[0]))
        self.scaleView(0.0)

    def resizeEvent(self, QResizeEvent):  # also gets called by scrollbars appearing!
        super(TIFView, self).resizeEvent(QResizeEvent)
        self.update_minZoom()
        # print 'Resize, ', QResizeEvent.oldSize(), QResizeEvent.size()
        if self.transform().m11() < self.minZoom:
            self.scaleView(0.0)

    def update_minZoom(self):
        sceneRect = self.sceneRect()
        self.minZoom = min(((self.width() - 2) / max(1, sceneRect.width()),
                            (self.height() - 2) / max(1,
                                                      sceneRect.height()))) - 10 ** -6  # minus a bit, since checking floats

    def setTIFpixmap(self, pixmap):
        self.TIFpixmap.setPixmap(pixmap)

    def scaleView(self, scaleFactor):
        assert abs(self.transform().m11()) == abs(self.transform().m22())

        pre_zoom = self.transform().m11()
        post_zoom = pre_zoom * scaleFactor

        if post_zoom < self.minZoom:
            scaleFactor = self.minZoom / pre_zoom
        elif post_zoom > self.maxZoom:
            scaleFactor = self.maxZoom / pre_zoom
        self.scale(scaleFactor, scaleFactor)

    def wheelEvent(self, event):
        self.scaleView(math.pow(2.0, event.delta() / 240.0))

    def fromTIFView(self, targetTIFView):
        """ Copies the view of another target TIFview
        """
        # TODO, just use copy??? or does qt not like that....
        self.TIF = targetTIFView.TIF
        sceneRect = targetTIFView.scene().sceneRect().getRect()
        self.scene().setSceneRect(*sceneRect)  # otherwise when switching to a new tif scene is off
        self.setSceneRect(self.scene().sceneRect())
        self.centerOn(sceneRect[0] + sceneRect[2] * .5, sceneRect[1] + sceneRect[3] * .5)
        self.minZoom = min(((self.width() - 2) / sceneRect[2],
                            (self.height() - 2) / sceneRect[3])) - 10 ** -6  # plus a bit, since checking floats
        # print ((self.width()-2)/sceneRect[2], (self.height()-2)/sceneRect[3])
        self.TIFpixmap.setPos(sceneRect[0], sceneRect[1])

        # self.TIFpixmap.setScale(min(sceneRect[2]/TIFshape[1], sceneRect[3]/TIFshape[0]))
        self.TIFpixmap.setScale(targetTIFView.TIFpixmap.scale())
        ## print (sceneRect[2]/TIFshape[1], sceneRect[3]/TIFshape[0])
        self.scaleView(0.0)
        self.TIFpixmap.setPixmap(targetTIFView.TIFpixmap.pixmap())

    def keyPressEvent(self, QKeyEvent):
        if not QKeyEvent.isAutoRepeat():
            if QKeyEvent.key() == QtCore.Qt.Key_Space:
                self.setDragMode(self.ScrollHandDrag)
        super(TIFView, self).keyPressEvent(QKeyEvent)

    def keyReleaseEvent(self, QKeyEvent):
        if not QKeyEvent.isAutoRepeat():
            if QKeyEvent.key() == QtCore.Qt.Key_Space:
                self.setDragMode(self.NoDrag)
        super(TIFView, self).keyReleaseEvent(QKeyEvent)

    def enterEvent(self, *args, **kwargs):
        self.setFocus()
        super(TIFView, self).enterEvent(*args, **kwargs)


class TIFViewZ(TIFView, object):
    levelChanged = QtCore.Signal(int)
    maxLevelChanged = QtCore.Signal(int)
    mouseMoved = QtCore.Signal(QtCore.QPointF, name='positionChanged')

    def __init__(self, parent):
        super(TIFViewZ, self).__init__(parent)
        self.zlevel = None
        self.TIF = None

    def add_slider(self, slider):
        # self.slider = slider
        slider.valueChanged.connect(self.setzlevel)
        self.levelChanged.connect(slider.setValue)
        self.maxLevelChanged.connect(slider.setMaximum)

    def newTIF(self, TIFstack, sceneRect=None):
        self.TIF = TIFstack
        TIFshape = self.TIF.shape
        if sceneRect is None:
            sceneRect = (0, 0, self.TIF.shape[1], self.TIF.shape[0])
        # print sceneRect, TIFshape
        self.scene().setSceneRect(*sceneRect)  # otherwise when switching to a new tif scene is off
        self.setSceneRect(self.scene().sceneRect())
        self.centerOn(sceneRect[0] + sceneRect[2] * .5, sceneRect[1] + sceneRect[3] * .5)
        self.minZoom = min(((self.width() - 2) / sceneRect[2],
                            (self.height() - 2) / sceneRect[3])) - 10 ** -6  # minus a bit, since checking floats
        self.TIFpixmap.setPos(sceneRect[0], sceneRect[1])
        self.TIFpixmap.setScale(min(sceneRect[2] / TIFshape[1], sceneRect[3] / TIFshape[0]))
        self.scaleView(0.0)
        self.maxLevelChanged.emit(self.TIF.shape[2] - 1)
        self.setzlevel(0)

    def setzlevel(self, zlevel):
        if zlevel != self.zlevel:
            self.zlevel = zlevel
            a = self.TIF[:, :, zlevel].astype('uint8')
            QI = QtGui.QImage(a.data, a.shape[1], a.shape[0], a.shape[1], QtGui.QImage.Format_Indexed8)
            pixmap = QtGui.QPixmap.fromImage(QI)
            self.setTIFpixmap(pixmap)
            self.levelChanged.emit(zlevel)
            # print self.TIFpixmap.pixmap().depth()

    def mouseMoveEvent(self, QMouseEvent):
        pos_scene = self.mapToScene(QMouseEvent.pos())
        self.mouseMoved.emit(pos_scene)
        # self.window().updatePosLabels(pos_scene)
        super(TIFViewZ, self).mouseMoveEvent(QMouseEvent)

        # TODO labels for showing Z level?
