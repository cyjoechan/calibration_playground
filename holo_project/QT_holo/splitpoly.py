from __future__ import division
import math
import numpy as np
from collections import namedtuple
from scipy import optimize
from shapely.geometry import Polygon, Point, LineString
from shapely.ops import polygonize


def makevec(magnitude, angle):
    y = math.sin(math.radians(angle)) * magnitude
    x = math.cos(math.radians(angle)) * magnitude
    return np.array([x, y])


def splitpoly(poly, angle=0, n=2, distance=40):
    if not isinstance(poly, Polygon):
        poly = Polygon(poly)

    newpolys = []
    splittinglines = []

    spreadpoint = np.asarray(poly.centroid) - makevec(distance, angle)

    target_area = poly.area / float(n)
    target_poly = poly
    for i in range(n - 1):
        def parallel_splitting_line(offset):
            point = np.asarray(target_poly.centroid) + np.squeeze(makevec(offset, angle - 90))  # orthgonal vector
            # print 'point: ', point, ' centroid: ', target_poly.centroid
            b = target_poly.bounds
            vec = makevec(b[2] - b[0] + b[3] - b[1], angle)
            # the bounds math is width + height, which should make a long enough line
            p1 = point + vec
            p2 = point - vec
            return LineString([p1, p2])

        def radial_splitting_line(offset):
            point = np.asarray(poly.centroid) + np.squeeze(makevec(offset, angle - 90))  # orthgonal vector
            # print 'point: ', point, ' centroid: ', target_poly.centroid
            b = target_poly.bounds
            angleoffset = math.degrees(math.atan2(offset, distance))
            vec = makevec((b[2] - b[0] + b[3] - b[1]) * 2, angle - angleoffset)
            # the bounds math is width + height, which should make a long enough line
            p1 = point + vec
            p2 = point - vec
            return LineString([p1, p2])

        def thing(offset):
            line = radial_splitting_line(offset)
            splitpolys = list(polygonize(target_poly.boundary.union(line)))
            if len(splitpolys) == 1:
                return 10 ** 7
            smaller_poly = min(splitpolys, key=lambda f: f.area)
            return abs(target_area - smaller_poly.area)

        if i == 0:
            startoffset = 0
        else:
            # alpha = math.atan2(target_poly.centroid.x-spreadpoint[0], target_poly.centroid.y-spreadpoint[1])
            # startoffset = distance * math.tan(alpha-math.radians(angle-180))
            # # if math.degrees(alpha) % 360 > angle % 360:
            # #     startoffset *= -1
            # # print i, alpha, startoffset
            # startoffset = poly.centroid.distance(target_poly.centroid)
            # angleline = LineString([spreadpoint, poly.centroid])

            # s = np.array(poly.centroid) - spreadpoint
            # v = np.array(target_poly.centroid) - spreadpoint
            # startoffset = np.linalg.norm(np.dot(np.dot(v,s)/np.dot(s,s), s))

            # dist = poly.centroid.distance(target_poly.centroid)
            # dist #distance new centroids to centroid-splitpoint line
            # startoffset = dist/math.sqrt(1-(dist/distance)**2)
            a = np.array(poly.centroid) - spreadpoint
            b = np.array(target_poly.centroid) - spreadpoint
            startoffset = distance * math.tan(math.acos(np.dot(a, b) / (np.linalg.norm(a) * np.linalg.norm(b))))

            if thing(startoffset) > thing(-startoffset):
                startoffset = -startoffset

        # res = optimize.minimize_scalar(thing, options={'xtol':.01}).x
        res = 0
        try:
            res = optimize.newton(thing, startoffset)  # fast but can fail for the complex case
        except RuntimeError:
            pass

        if thing(res) > .01:
            print('flipped startoffset')
            try:
                res = optimize.newton(thing, -startoffset)
            except RuntimeError:
                pass

        if thing(res) > .01:
            print('spaced offsets')
            b = target_poly.bounds
            widthheight = 2 * max((b[2] - b[0]), (b[3] - b[1]))
            startoffsets = np.linspace(-widthheight, +widthheight, 200)
            startoffset = startoffsets[np.vectorize(thing)(startoffsets).argmin()]
            res = optimize.newton(thing, startoffset)
            # import pylab
            # x = np.linspace(-40, 40, 1000)
            # pylab.plot(x, np.vectorize(thing)(x))
            # pylab.axvline(res, c='red')
            # pylab.show()

        if thing(res) > .01:
            print('basinhopping')
            res = optimize.basinhopping(thing, startoffset, 3, T=10.0).x[0]

        line = radial_splitting_line(res)
        polys = sorted(polygonize(target_poly.boundary.union(line)), key=lambda f: f.area)
        if len(polys) == 1:
            # import pylab
            # x = np.linspace(-40, 40, 1000)
            # pylab.plot(x, np.vectorize(thing)(x))
            # pylab.axvline(res, c='red')
            # pylab.show()
            raise AssertionError
        if len(polys) > 2:
            raise AssertionError
        newpolys.append(polys[0])
        target_poly = polys[1]
        # TODO should be union of polys[1:], in case there's a curvy concave shape
        splittinglines.append(line)

    newpolys.append(target_poly)

    print(target_area, 'target, errors: ', [p.area - target_area for p in newpolys])
    assert (np.mean([p.area for p in polys]) - target_area) < poly.area / 1000
    SplitPolyResult = namedtuple('SplitPolyResult', ['polys', 'splittinglines', 'centroid', 'spreadpoint'])
    result = SplitPolyResult(newpolys, splittinglines, poly.centroid, spreadpoint)
    return result


if __name__ == '__main__':
    # print makevec(5, 0)
    # print makevec(5, 45)
    # print makevec(5, 90)
    # print makevec(5, 180)
    # print makevec(0, -90)

    import pylab

    x = np.linspace(-370, 370, 1000)
    y = np.asarray([makevec(5, i) for i in x])
    pylab.plot(x, y[:, 0])
    pylab.plot(x, (y ** 2).sum(1) ** .5)
    pylab.show()

    # poly = Polygon([(0,0),(5,5),(5,0)])
    testpoly = [(0, 0), (5, 5), (5, 0)]
    n = 5
    newpolys = splitpoly(testpoly, 0, n=n)
    print('')
    print(newpolys)
    print([p.area for p in newpolys])
    print(Polygon(testpoly).area / n)
