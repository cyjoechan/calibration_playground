import numpy as np
from qtpy import QtGui, QtWidgets, QtCore
from qt_holo.ui_split_dialog import Ui_Dialog
from qt_holo.splitpoly import splitpoly


class SplitDialog(QtWidgets.QDialog, Ui_Dialog):
    def __init__(self, nodes, drawWidget):
        super(SplitDialog, self).__init__()

        self.setupUi(self)
        self.poly = [(n.scenePos().x(), n.scenePos().y()) for n in nodes]  # scenePos != pos! Want scene coords here
        self.splitpolys = None

        self.splitView.fromTIFView(drawWidget)

        self.angleDial.valueChanged.connect(self.split)
        self.numSlider.valueChanged.connect(self.numSliderChanged)
        self.distanceSlider.valueChanged.connect(self.split)
        self.numSliderChanged()  # to set label and start with a splitted poly

    def numSliderChanged(self):
        self.numSectionsLabel.setText(str(self.numSlider.value()))
        self.split()

    def split(self):
        self.splitView.clear()
        try:
            res = splitpoly(self.poly, self.angleDial.value() - 90, self.numSlider.value(),
                            self.distanceSlider.value() / 10.)
        except (RuntimeError, AssertionError):
            self.splittingFailedLabel.setVisible(True)
        else:
            self.splittingFailedLabel.setVisible(False)

            self.splitpolys = res.ROIs
            pen = QtGui.QPen(QtGui.QColor('Blue'))
            pen.setWidthF(1)
            for poly in self.splitpolys:
                points = [QtCore.QPointF(point[0], point[1]) for point in np.asarray(poly.boundary).tolist()]
                self.splitView.scene().addPolygon(QtGui.QPolygonF(points), pen)

            if 1:
                color = QtGui.QColor('Red')
                color.setAlpha(100)
                pen = QtGui.QPen(color)
                for poly in res.splittinglines:
                    points = [QtCore.QPointF(point[0], point[1]) for point in np.asarray(poly.boundary).tolist()]
                    self.splitView.scene().addPolygon(QtGui.QPolygonF(points), pen)

                s = res.spreadpoint
                pen = QtGui.QPen(QtGui.QColor('White'))
                self.splitView.scene().addEllipse(s[0] - 1, s[1] - 1, 2, 2, pen)

                s = np.array(res.centroid)
                pen = QtGui.QPen(QtGui.QColor('Red'))
                self.splitView.scene().addEllipse(s[0] - 1, s[1] - 1, 2, 2, pen)

    @staticmethod
    def run(*args, **kwargs):
        d = SplitDialog(*args, **kwargs)
        res = d.exec_()
        polys = d.splitpolys
        polys = [np.asarray(p.boundary).tolist() for p in polys]
        result = res == d.Accepted
        return result, polys


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication(sys.argv)
    # d = SplitDialog()
    # d.show()
    print(SplitDialog.run([(0, 0), (5, 5), (5, 0)]))
    app.exec_()
