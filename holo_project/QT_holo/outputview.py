from qtpy import QtGui, QtCore, QtSvg
from qt_holo.TIFView import TIFViewZ
from qt_holo import baseROI
from qt_holo.ROIset import ROIset
import numbers

timescalefactor = 10.0  # For time slider in the output view


class OutputView(TIFViewZ, object):
    def __init__(self, parent):
        super(OutputView, self).__init__(parent)
        self.newROIs = None
        self.time = 0
        self.playing = False
        self.animation = None

    def setNewROIS(self, newROIs):
        assert isinstance(newROIs, ROIset)
        if self.newROIs:
            [newROI.deleteLater() for newROI in self.newROIs]
        self.newROIs = newROIs
        [self.scene().addItem(newROI) for newROI in newROIs]

        frames = newROIs.generateSVGset()
        self.window().holoclient.generate(frames, wavelength=self.window().wavelengthSpinBox.value(),
                                          correction_factor=self.window().SLMcorrSpinBox.value())

        self.window().outputTimeSlider.setMaximum(self.newROIs.duration * timescalefactor)
        self.doupdate()
        self.scene().update()
        self.setzlevel(0)

    def clearROIs(self):
        [r.deleteLater() for r in self.ROIs]

    def showoutofZ(self):
        return self.window().showOutOfZ.isChecked()

    def showoutoftime(self):
        return self.window().showOutOfTime.isChecked()

    def doupdate(self):
        """
        Just calling update on the graphics view doesn't seem to redraw, but calling it on the viewport does
        wasn't sure if overriding update to always redraw the viewport as well would be inefficent
        """
        self.viewport().update()

    def updateOutputTime(self, time):
        self.time = time / timescalefactor  # sliders are only int
        self.doupdate()
        self.window().outputLabelTiming.setText("%.2f seconds" % self.time)
        # TODO should switch to signal/slot approach

    @property
    def ROIs(self):
        return [item for item in self.scene().items() if isinstance(item, baseROI.BasePoweredROI)]

    def play(self):
        if self.playing:
            self.animation.stop()
            self.playing = False
        else:
            timeslider = self.window().outputTimeSlider
            animation = QtCore.QPropertyAnimation(timeslider, "value")
            animation.setDuration(timeslider.maximum() / timescalefactor * 1000)  # in msec
            animation.setStartValue(timeslider.minimum())
            animation.setEndValue(timeslider.maximum())
            animation.start()
            self.animation = animation
            self.playing = True

    @property
    def maxrawpower(self):
        return max([r.maxpower for r in self.ROIs])

        # TODO overlaying ROIs/analysis tool
