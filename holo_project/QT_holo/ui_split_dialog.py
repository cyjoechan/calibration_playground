# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_split_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(461, 539)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(260, 500, 181, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.angleDial = QtWidgets.QDial(Dialog)
        self.angleDial.setGeometry(QtCore.QRect(20, 410, 131, 111))
        self.angleDial.setMaximum(359)
        self.angleDial.setWrapping(True)
        self.angleDial.setNotchTarget(45.0)
        self.angleDial.setNotchesVisible(True)
        self.angleDial.setObjectName("angleDial")
        self.splitView = SplitView(Dialog)
        self.splitView.setGeometry(QtCore.QRect(10, 10, 441, 351))
        self.splitView.setObjectName("splitView")
        self.numSlider = QtWidgets.QSlider(Dialog)
        self.numSlider.setGeometry(QtCore.QRect(179, 450, 241, 20))
        self.numSlider.setMinimum(2)
        self.numSlider.setMaximum(20)
        self.numSlider.setPageStep(1)
        self.numSlider.setOrientation(QtCore.Qt.Horizontal)
        self.numSlider.setTickPosition(QtWidgets.QSlider.TicksBelow)
        self.numSlider.setTickInterval(1)
        self.numSlider.setObjectName("numSlider")
        self.numSectionsLabel = QtWidgets.QLabel(Dialog)
        self.numSectionsLabel.setGeometry(QtCore.QRect(340, 430, 46, 13))
        self.numSectionsLabel.setObjectName("numSectionsLabel")
        self.sectionsLabel = QtWidgets.QLabel(Dialog)
        self.sectionsLabel.setGeometry(QtCore.QRect(230, 430, 101, 16))
        self.sectionsLabel.setObjectName("sectionsLabel")
        self.label_3 = QtWidgets.QLabel(Dialog)
        self.label_3.setGeometry(QtCore.QRect(20, 390, 61, 16))
        self.label_3.setObjectName("label_3")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(180, 390, 46, 13))
        self.label.setObjectName("label")
        self.distanceSlider = QtWidgets.QSlider(Dialog)
        self.distanceSlider.setGeometry(QtCore.QRect(239, 390, 181, 20))
        self.distanceSlider.setMinimum(30)
        self.distanceSlider.setMaximum(2000)
        self.distanceSlider.setPageStep(100)
        self.distanceSlider.setProperty("value", 600)
        self.distanceSlider.setOrientation(QtCore.Qt.Horizontal)
        self.distanceSlider.setObjectName("distanceSlider")
        self.splittingFailedLabel = QtWidgets.QLabel(Dialog)
        self.splittingFailedLabel.setEnabled(False)
        self.splittingFailedLabel.setGeometry(QtCore.QRect(30, 130, 381, 81))
        self.splittingFailedLabel.setTextFormat(QtCore.Qt.RichText)
        self.splittingFailedLabel.setWordWrap(True)
        self.splittingFailedLabel.setObjectName("splittingFailedLabel")

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.numSectionsLabel.setText(_translate("Dialog", "#"))
        self.sectionsLabel.setText(_translate("Dialog", "Number of sections:"))
        self.label_3.setText(_translate("Dialog", "Split Angle:"))
        self.label.setText(_translate("Dialog", "Distance:"))
        self.splittingFailedLabel.setText(_translate("Dialog", "<html><head/><body><p align=\"center\"><span style=\"\n"
"                    font-size:14pt; font-weight:600; color:#ff0000;\">Splitting failed for these parameter\n"
"                    settings.</span></p></body></html>\n"
"                "))

from splitView import SplitView
