import itertools
from qt_holo.paintSVG import generateSVG
from holographics.frame import Frame


class ROIset(list):
    def __init__(self, ROIs=None):
        super(ROIset, self).__init__()
        if ROIs is not None:
            self.extend(ROIs)

    # @property
    # def ROIs(self):
    #     return list(self)  # this is a bit wierd, should cast back into normal list?

    def generateSVGset(self):  # things to pass in Zdiffs for each ROI - make ROI method/prop
        frames = []
        frame_num = 0
        keytimes = self.keytimes
        for startkeytime, stopkeytime in zip(keytimes[:-1], keytimes[1:]):
            for Zlevel in self.Zlevels:
                svg = generateSVG(self, 'blank', startkeytime, Zlevel)
                Zdiff = 42  # self.window().TIFmeta.Z(Zlevel) - self.window().objZlevel #TODO need Zlevel
                # diff in um between the ROI and objective

                frames += [Frame(svg, frame_num=frame_num, Zlevel=Zdiff, duration=stopkeytime - startkeytime)]

                # TODO frametime expansion stuff -?
            frame_num += 1
        return frames

    @property
    def maxrawpower(self):
        """Maximum power of all the individual newROIs, to assist in normalizing for display/export
        """
        return max([ROI.max for ROI in self])

    @property
    def duration(self):
        return self.keytimes[-1]

    @property
    def keytimes(self):
        times = list(set(itertools.chain(*[ROI.times for ROI in self])))
        times.sort()
        return times

    @property
    def Zlevels(self):
        return list(set([p.ZLevel for p in self]))


if __name__ == '__main__':
    t = ROIset()
    t.append(2)
