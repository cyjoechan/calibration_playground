from qtpy import QtGui, QtCore, QtSvg, QtWidgets
from qt_holo.TIFView import TIFView, TIFViewZ
from qt_holo.polyROI import PolyNode, PolyROI
from qt_holo import baseROI


class DrawWidget(TIFView):
    def __init__(self, parent):
        super(DrawWidget, self).__init__(parent)
        self.currentlyDrawing = False
        self.currentPoly = None
        self.drawpolyclass = PolyROI

    def clearROIs(self):
        [r.deleteLater() for r in self.ROIs]
        self.finishDrawing(cancel=True)

    @property
    def ROIs(self):
        return [c for c in self.scene().items() if isinstance(c, baseROI.BaseInteractionROI)]

    def finishDrawing(self, cancel=False):
        self.viewport().setCursor(QtCore.Qt.ArrowCursor)  # need viewport, see QTBUG-4190

        if self.currentlyDrawing:
            self.currentlyDrawing = False
            if cancel or len(self.currentPoly) < 3:
                self.scene().removeItem(self.currentPoly)
                self.currentPoly.deleteLater()
            self.currentPoly = None

    def keyPressEvent(self, QKeyEvent):
        if QKeyEvent.key() == QtCore.Qt.Key_Escape:
            self.finishDrawing(cancel=True)
        elif QKeyEvent.key() == QtCore.Qt.Key_Return:
            self.finishDrawing()

    def mouseDoubleClickEvent(self, QMouseEvent):
        super(DrawWidget, self).mouseDoubleClickEvent(QMouseEvent)
        self.finishDrawing()

    def mousePressEvent(self, event):
        if self.window().TIF is None:
            self.window().open()
            return

        if not self.currentlyDrawing:
            super(DrawWidget, self).mousePressEvent(event)
            for item in self.items(event.pos()):
                if isinstance(item, baseROI.BaseInteractionROI) and item.atCurrentZLevel or isinstance(item,
                                                                                                       baseROI.BaseInteractionROI):
                    return
                    # print self.itemAt(event.pos()), self.itemAt(self.mapToScene(event.pos()).toPoint())

        if event.buttons() == QtCore.Qt.LeftButton:
            if not self.currentlyDrawing:
                self.currentlyDrawing = True
                self.viewport().setCursor(QtCore.Qt.CrossCursor)
                self.currentPoly = self.drawpolyclass(self.window().currentZLevel)
                self.scene().addItem(self.currentPoly)

            self.currentPoly.addNode(self.mapToScene(event.pos()))
            self.currentPoly.update()

        else:  # right or middle click
            self.finishDrawing()

    def mouseMoveEvent(self, QMouseEvent):
        pos_scene = self.mapToScene(QMouseEvent.pos())
        self.window().updatePosLabels(pos_scene)
        super(DrawWidget, self).mouseMoveEvent(QMouseEvent)

    # def keyPressEvent(self, QKeyEvent):
    # #     print 'k', self.scene().sceneRect()
    # #     self.scene().setSceneRect(-150, -150, 300, 300)
    # #     self.fitInView(self.scene().sceneRect())
    # #     self.minZoom = .5

    def scaleView(self, *args, **kwargs):
        super(DrawWidget, self).scaleView(*args, **kwargs)

        self.window().zoomInAct.setEnabled(self.transform().m11() < self.maxZoom)
        self.window().zoomOutAct.setEnabled(self.transform().m11() > self.minZoom)

        # update position labels
        pos_scene = self.mapToScene(self.mapFromGlobal(QtGui.QCursor.pos()))
        self.window().updatePosLabels(pos_scene)


class DrawWidgetZ(TIFViewZ):
    def __init__(self, parent):
        super(DrawWidgetZ, self).__init__(parent)
        self.currentlyDrawing = False
        self.currentPoly = None
        self.drawpolyclass = PolyROI
        self.copytarget = None
        self.pasteAction = QtWidgets.QAction("&Paste", self, shortcut="Ctrl+V", triggered=self.paste)
        # self.addAction(self.pasteAction)
        parent.addAction(self.pasteAction)

    def clearROIs(self):
        [r.deleteLater() for r in self.ROIs]
        self.finishDrawing(cancel=True)

    @property
    def ROIs(self):
        return [c for c in self.scene().items() if isinstance(c, baseROI.BaseInteractionROI)]

    def finishDrawingDummy(self, *args):
        self.finishDrawing()  # To get hooked up to external events etc.

    def finishDrawing(self, cancel=False):
        if self.currentlyDrawing:
            self.viewport().setCursor(QtCore.Qt.ArrowCursor)  # need viewport, see QTBUG-4190
            self.currentlyDrawing = False
            if cancel or len(self.currentPoly) < 3:
                self.scene().removeItem(self.currentPoly)
                self.currentPoly.deleteLater()
            else:
                self.currentPoly.changedZ()
            self.currentPoly = None

    def add_slider(self, slider):
        slider.valueChanged.connect(self.finishDrawingDummy)
        super(DrawWidgetZ, self).add_slider(slider)

    def keyPressEvent(self, QKeyEvent):
        if QKeyEvent.key() == QtCore.Qt.Key_Escape:
            self.finishDrawing(cancel=True)
        elif QKeyEvent.key() == QtCore.Qt.Key_Return:
            self.finishDrawing()
        else:
            super(DrawWidgetZ, self).keyPressEvent(QKeyEvent)

    def mouseDoubleClickEvent(self, QMouseEvent):
        super(DrawWidgetZ, self).mouseDoubleClickEvent(QMouseEvent)
        self.finishDrawing()

    def mousePressEvent(self, event):
        if self.TIF is None:
            return

        if self.currentlyDrawing:
            if event.buttons() == QtCore.Qt.LeftButton:
                self.currentPoly.addNode(self.mapToScene(event.pos()))
                self.currentPoly.update()
            else:  # right or middle click
                self.finishDrawing()
        else:
            super(DrawWidgetZ, self).mousePressEvent(event)

        if not event.isAccepted():  # if not being handled by an ROI widget already
            if event.buttons() == QtCore.Qt.LeftButton:
                if not self.currentlyDrawing:
                    self.currentlyDrawing = True
                    self.viewport().setCursor(QtCore.Qt.CrossCursor)
                    self.currentPoly = self.drawpolyclass(self.zlevel)
                    self.scene().addItem(self.currentPoly)
                self.currentPoly.addNode(self.mapToScene(event.pos()))
                self.currentPoly.update()

    def addrois(self, rois):
        [self.scene().addItem(r) for r in rois]

    def setzlevel(self, zlevel):
        super(DrawWidgetZ, self).setzlevel(zlevel)
        [roi.changedZ(zlevel) for roi in self.ROIs]

    def setcopytarget(self, roi):
        self.copytarget = roi

    def paste(self):
        self.finishDrawing()
        if self.copytarget:
            d = self.copytarget.todict()
            d['z'] = self.zlevel
            newroi = PolyROI.fromdict(d)
            # current mouse pos in scene coords: self.mapToScene(self.mapFromGlobal(QtGui.QCursor().pos()))
            currentmouse = self.mapToScene(self.mapFromGlobal(QtGui.QCursor().pos())).toTuple()
            xshift = currentmouse[0] - newroi.centroid[0]
            yshift = currentmouse[1] - newroi.centroid[1]
            [n.setPos(n.pos() + QtCore.QPointF(xshift, yshift)) for n in newroi.nodes]
            self.scene().addItem(newroi)
