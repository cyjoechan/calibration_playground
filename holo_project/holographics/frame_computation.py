import math
import os

import numpy as np
import scipy.ndimage
from joblib import Memory
from scipy.misc import imresize

from holographics import svg_util
from holographics.GSF_3D import GS_3D as GS
from holographics.frame import Frame, target_size, compute_size, svg_target_size_x, svg_target_size_y
from holographics.diffraction_efficiency import diff3d as diffractioneff3d

cachedir = './gsf_cache'
memory = Memory(cachedir=cachedir, verbose=0)

if not os.path.exists(cachedir):
    os.mkdir(cachedir)
memory.clear()


def computehologram(frames, wavelength, *args, **kwargs):
    target_amplitudes, Zs = zip(*[[f.raster, f.Zlevel] for f in frames])

    # drop blank frames, if all are blank, use a large blank circle
    non_blank = [(t, Z) for t, Z in zip(target_amplitudes, Zs) if t.sum() != 0]
    if non_blank:
        target_amplitudes, Zs = zip(*non_blank)
    else:
        print('Frames blank, showing default large circle')
        f = Frame(svg=svg_util.generate_circle_svg(0, 0, 80))
        f.rasterize()
        target_amplitudes = [f.raster]
        Zs = [0]

    res = GS(target_amplitudes, Zs, wavelength, *args, **kwargs)

    hologram = (res.phase % (2 * math.pi)) * 255 / (2 * math.pi)

    # resize to match the SLM size
    resized = imresize(hologram, (max(target_size),) * 2)
    resized = resized[:, int(resized.shape[1] / 2 - target_size[1] / 2):int(resized.shape[1] / 2 + target_size[1] / 2)].astype(
        'uint8')
    return (resized)
    # return (res)


#
# def computemultipatternhologram(target_amplitudes, Zs, wavelength=960, npatterns=1, shiftdistance=1, *args, **kwargs):
#     holo = memory.cache(computehologram)(target_amplitudes, Zs, wavelength, *args, **kwargs)

def computemultipatternhologram(frames, wavelength, *args, **kwargs):
    holo = memory.cache(computehologram)(frames, wavelength, *args, **kwargs)
    return [holo]

    # assert 1 <= npatterns <= 32, 'Must have at least one pattern, and less than 32'
    #
    # # shift averaging to reduce speckle:
    # if npatterns == 1:
    #     return [holo]
    # else:
    #     shifts = [(shiftdistance * np.cos(v), shiftdistance * np.sin(v)) for v in
    #               np.linspace(0, 2 * np.pi, npatterns, endpoint=False)]
    #     holos = [scipy.ndimage.interpolation.shift(holo, shift, mode='reflect', order=3) for shift in shifts]
    #     return holos


def simple_generate_frames(frames):
    """
    Only for testing purposes
    """
    for f in frames:
        f.holograms = computemultipatternhologram([f.raster], [f.Zlevel], npatterns=1)


def frame_diffraction_effs(frames):
    """
    Correct frames to compensate for diffraction efficiency
    """
    lx = np.linspace(-svg_target_size_x / 2, svg_target_size_x / 2, compute_size[0])
    ly = np.linspace(-svg_target_size_y / 2, svg_target_size_y / 2, compute_size[1])
    x, y = np.meshgrid(lx, ly)

    for f in frames:
        diff = diffractioneff3d(x, y, f.Zlevel)
        f.raster = f.raster / diff
    fmax = max([f.raster.max() for f in frames])
    for f in frames:
        f.raster = f.raster * 255 / fmax
        f.raster = f.raster.astype(np.uint8)
