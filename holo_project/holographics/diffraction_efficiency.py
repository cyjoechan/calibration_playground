from __future__ import division
import numpy as np
import scipy.special

Si = lambda x: scipy.special.sici(x)[0]
sinc = np.sinc
pi = np.pi
cos = np.cos


def diff3d(x, y, z, xyscale=.006, zscale=.002):
    """
    Smaller number for scale decreases the amount of correction
    """
    return sinc(xyscale * x / 2) ** 2 * sinc(xyscale * y / 2) ** 2 * sinc(zscale * z / 2) ** 2

