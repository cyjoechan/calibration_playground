import os
from joblib import Memory
from holographics.frame_computation import cachedir

if not os.path.exists(cachedir):
    os.mkdir(cachedir)
memory = Memory(cachedir=cachedir, verbose=0)
