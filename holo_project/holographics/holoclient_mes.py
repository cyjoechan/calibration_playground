from __future__ import print_function, division
import sys
import zmq

import serializer, holo_msg_pb2

context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect("tcp://O1-586:51233")


def send(cmdtype, *args):
    cmd = holo_msg_pb2.StandardCommand()
    cmd.cmd = getattr(holo_msg_pb2.StandardCommand, cmdtype)  # TODO stripwhitespace, etc?
    if cmd.cmd == holo_msg_pb2.StandardCommand.CALIBRATE_CIRCLE:
        cmd.calibration_circle_x = float(args[0])
        cmd.calibration_circle_y = float(args[1])

    elif cmd.cmd == holo_msg_pb2.StandardCommand.CALIBRATE_Z:
        cmd.calibration_Z_level = float(args[0])

    elif cmd.cmd == holo_msg_pb2.StandardCommand.CALIBRATE_Z_OBJ:
        cmd.objectiveZlevel = float(args[0])

    elif cmd.cmd == holo_msg_pb2.StandardCommand.AUTOFOCUS:
        cmd.calibration_Z_level = float(args[0])
        cmd.objectiveZlevel = float(args[1])

    msg = serializer.serialize(cmd)
    socket.send_multipart(msg)
    reply = socket.recv_multipart()
    cmdtype, framelist = serializer.unserialize(reply, holo_msg_pb2.StandardReply())
    print(str(cmdtype.new_obj_z_level))
    return cmdtype


if __name__ == '__main__':
    assert len(sys.argv) > 1
    send(*sys.argv[1:])  # argv[0] is the command name
