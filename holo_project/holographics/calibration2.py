from __future__ import print_function, division
import os.path
from collections import namedtuple
import pickle
import warnings

import numpy as np
import cv2
from matplotlib import pyplot as plt
import matplotlib.gridspec as gridspec
import scipy.ndimage
import scipy.stats
from ximea import xiapi

from holographics import svg_util, transformations
from holographics.frame import Frame
from holographics.pickimages import pickimage

import scipy.misc


def pad_zeroes(array):
    return np.c_[array, np.zeros((array.shape[0], 1), 'float32')]


Zscalefactor = 1  # Default scaling for Z before calibration is performed

Calib = namedtuple('Calib', ['rms', 'camera_matrix', 'dist_coefs', 'rvecs', 'tvecs'])


class cvCameraHandle(object):
    """
    Holds handle to the camera, uses openCV
    """

    def __init__(self, video_src=0):
        self.video_src = video_src
        self.cam = None

    def grab_image(self):
        """
        Grabs an image.
        Connects to the camera is necessary
        """
        if not self.cam or not self.cam.isOpened():
            self.start_cam()

        self.cam.read()  # seems like sometimes some buffer is stuck with an old image, throw away a grab to clear
        ret, frame = self.cam.read()
        if ret:
            return frame[:, :, 0]
        else:
            raise Exception("Couldn't read from camera properly")

    def grab_image_quick(self):
        """
        Grabs an image.
        Doesn't open camera handle, and doescalibrate_XYn't double grab to prevent the buffering issue
        """
        ret, frame = self.cam.read()
        if ret:
            return frame[:, :, 0]
        else:
            raise Exception("Couldn't read from camera properly")

    def start_cam(self):
        self.cam = cv2.VideoCapture(self.video_src)
        if self.cam is None or not self.cam.isOpened():
            raise Exception("Couldn't open camera for calibration!")
        self.cam.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH, 1280)
        self.cam.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, 1024)

    def release_cam(self):
        self.cam.release()


class XimeaCameraHandle(object):
    """
    Holds handle to the camera, uses ximea api
    """

    def __init__(self, video_src=0):
        self.cam = None
        self.img = None
        self.cam_started = False

    def grab_image(self):
        """
        Grabs an image.
        Connects to the camera is necessary
        """
        if not self.cam_started:
            self.start_cam()
        self.cam.get_image(self.img)
        return self.img.get_image_data_numpy()

        # self.cam.read()  # seems like sometimes some buffer is stuck with an old image, throw away a grab to clear
        # ret, frame = self.cam.read()
        # if ret:
        #     return frame[:, :, 0]
        # else:
        #     raise Exception("Couldn't read from camera properly")

    # def grab_image_quick(self):
    #     """
    #     Grabs an image.
    #     Doesn't open camera handle, and doesn't double grab to prevent the buffering issue
    #     """
    #     ret, frame = self.cam.read()
    #     if ret:
    #         return frame[:, :, 0]
    #     else:
    #         raise Exception("Couldn't read from camera properly")

    def start_cam(self):
        cam = xiapi.Camera()
        print('Opening first ximea camera...')
        cam.open_device()
        # if self.cam is None or not self.cam.isOpened():
        #     raise Exception("Couldn't open camera for calibration!")
        cam.set_exposure(56)
        print('Exposure was set to %i us' % cam.get_exposure())
        img = xiapi.Image()
        cam.start_acquisition()
        # get data and pass them from camera to img
        self.cam = cam
        self.img = img
        self.cam_started = True

    def release_cam(self):
        self.cam.stop_acquisition()
        self.cam.close_device()
        print('Stopped camera acquisition...')
        self.cam_started = False


class Calibrator(object):
    def __init__(self, camerahandle, holobase):
        self.background = None
        self.camerahandle = camerahandle
        self.holobase = holobase
        self.holo_image = None  # for use with callback during playing

    def grab_point(self, x, y, z, size=20, correction_factor=None):
        return self.grab_svg(svg_util.generate_circle_svg(x, y, size), z, correction_factor)

    def grab_svg(self, svg, z, correction_factor=None):
        frame = Frame(svg=svg, Zlevel=z, duration=1, frame_num=0)
        frame.set_svg_bounds()
        frame.rasterize()
        self.holobase.generate_frames([frame], npatterns=1, correction_factor=correction_factor)
        self.holobase.frameplayer.playframes_with_callback(self.grab_holo_image, 0.4)
        return self.holo_image

    def findcenter(self, img, blurRadius=211):
        assert blurRadius % 2 == 1, "blurRadius must be odd!"

        img = img.copy()
        if self.background is not None:
            img = img.astype('int32')
            img -= self.background
            img = np.clip(img, 0, 2 ** 8)
            img = img.astype('uint8')

        img = cv2.GaussianBlur(img, (blurRadius,) * 2, 0, 0)
        centroid_int = np.unravel_index(img.argmax(), img.shape)
        # TODO could detect a poor fit, and raise an Exception
        centroid_int = (centroid_int[1], centroid_int[0])

        return centroid_int

    def findcenter2(self, img):
        circ = cv2.HoughCircles(img, method=cv2.cv.CV_HOUGH_GRADIENT, dp=1.2, minDist=100, minRadius=1, maxRadius=200,
                                param2=10)
        return circ[0, 0, 0:2]

    def grab_background(self):
        self.background = self.grab_image()

    def grab_image(self):
        return self.camerahandle.grab_image()

    def grab_holo_image(self):
        self.holo_image = self.grab_image()
        self.image_ok(self.holo_image)

    def load(self):
        assert self.filepath is not None
        if os.path.exists(self.filepath):
            try:
                with open(self.filepath, 'rb') as f:
                    return pickle.load(f)
            except:
                print("COULDN'T LOAD CALIBRATION! - SHV opened but failed")
                return None
        else:
            print("COULDN'T LOAD CALIBRATION! ", self.filepath)
            return None

    def save(self, data):
        with open(self.filepath, 'wb') as f:
            pickle.dump(data, f)

    def image_ok(self, img):
        assert img.dtype == np.dtype('uint8')
        overexposure = (img > 252).mean()
        underexposure = (img < 15).mean()

        print("Image over: %.3f and under %.3f exposure" % (overexposure, underexposure))
        return True


class CorrectionFactorCalibrator(Calibrator):
    def __init__(self, *args, **kwargs):
        self.correction_factor_range = .25
        self.correction_factor_middle = .75
        self.nspots = 10
        self.max_iterations = 1
        super(CorrectionFactorCalibrator, self).__init__(*args, **kwargs)

    def calibrate(self):
        correction_factor_middle = self.correction_factor_middle
        correction_factor_range = self.correction_factor_range
        for i in range(self.max_iterations):
            correction_factors = np.linspace(correction_factor_middle - correction_factor_range / 2.,
                                             correction_factor_middle + correction_factor_range / 2, self.nspots)
            factor_images = [self.grab_point(30, 30, 0, 24, factor) for factor in correction_factors]
            factor_images = [scipy.ndimage.interpolation.zoom(img, .5) for img in
                             factor_images]  # don't need full size images

            index = pickimage(factor_images, correction_factors)
            return correction_factors[index]


class XYCalibrator(Calibrator):
    def __init__(self, *args, **kwargs):
        self.filepath = 'holoXYcal.pkl'
        self.circle_images = []
        self.circle_positions = []
        super(XYCalibrator, self).__init__(*args, **kwargs)
        self.transformation_matrix = self.load()

    def grab_circle_image(self, position):
        self.circle_images.append(self.grab_image())
        self.circle_positions.append(position)

    def show_imgs(self, imgs, centers=None, title=None):
        gs = gridspec.GridSpec(2, 2)
        for i, img in enumerate(imgs):
            plt.subplot(gs[i])
            plt.imshow(img, 'gray')
            if centers is not None:
                plt.scatter(*centers[i], c='r', s=10)
        if title is not None:
            plt.title(title)
        plt.show()

    def run(self):
        circle_centers = [self.findcenter(img, 155) for img in self.circle_images]
        holo_imgs = [self.grab_point(pos[0], pos[1], 0) for pos in self.circle_positions]

        holo_centers = [self.findcenter(img, 431) for img in holo_imgs]

        calib1, err1 = transformations.fit_trans(np.asarray(self.circle_positions), np.asarray(circle_centers))
        calib2, err2 = transformations.fit_trans(np.asarray(holo_centers), np.asarray(self.circle_positions))

        print("Reprojection errors: ", err1, err2)
        calib = np.dot(transformations.pad_trans(calib2), transformations.pad_trans(calib1))

        cal_svgs = [self.apply(svg_util.generate_circle_svg(pos[0], pos[1], 20), calib) for pos in
                    self.circle_positions]
        # TODO savesvgs
        calibrated_holo_imgs = [self.grab_svg(svg, 0) for svg in cal_svgs]
        calibrated_holo_centers = [self.findcenter(img, 431) for img in calibrated_holo_imgs]

        print("Target circle centers", circle_centers)
        print("Uncalibrated circle centers", holo_centers)
        print("Calibrated circle centers", calibrated_holo_centers)

        um_per_pix = (np.abs(1 / calib1[0, 0]) + np.abs(1 / calib1[1, 1])) / 2.
        print('scaling um/pix %.4f avg, %.4f %.4f' % (
            um_per_pix, 1 / calib1[0, 0], 1 / calib1[1, 1]))
        rms_err_string = "RMS point mismatch %.3f (um)" % (
                transformations.rms(calibrated_holo_centers, circle_centers) * um_per_pix)
        print(rms_err_string)

        if 1:  # plot
            gs = gridspec.GridSpec(2, 2, top=.92, hspace=.1, wspace=.06, left=.02, right=.98)
            for i, pos in enumerate(self.circle_positions):
                plt.subplot(gs[i])
                plt.imshow(np.dstack(
                    [img / float(img.max()) for img in (self.circle_images[i], holo_imgs[i], calibrated_holo_imgs[i])]))
                plt.xlim([0, self.circle_images[i].shape[1]])
                plt.ylim([0, self.circle_images[i].shape[0]])

                for color, center in zip(('r', 'g', 'b'),
                                         (circle_centers[i], holo_centers[i], calibrated_holo_centers[i])):
                    plt.scatter(*center, c=color, s=100, alpha=1)

                plt.xlabel('Distance of imaging to calibrated %.2f (um)' % (
                        um_per_pix * transformations.rms(circle_centers[i], calibrated_holo_centers[i])))

            plt.figtext(.35, .95, 'Imaging points', color='red', ha='center')
            plt.figtext(.5, .95, 'Uncalibrated holo', color='green', ha='center')
            plt.figtext(.65, .95, 'Calibrated holo', color='blue', ha='center')
            plt.figtext(.5, .08, rms_err_string, ha='center')
            plt.suptitle('XY calibration report - close to continue')
            plt.show()  # eventually save as pdf? with datetimename

        self.transformation_matrix = calib
        self.save(self.transformation_matrix)

    def apply(self, svg, transform=None):
        if transform is not None:
            return svg_util.insert_transform(svg, transform)
        else:
            if self.transformation_matrix is None:
                print("Can't apply XYCalibration, not calibrated yet!")
                return svg
            return svg_util.insert_transform(svg, self.transformation_matrix)


class ZCalibrator(Calibrator):
    def __init__(self, *args, **kwargs):
        self.filepath = 'holoZcal.pkl'
        self.Zlevels = []
        self.Zobjs = []
        super(ZCalibrator, self).__init__(*args, **kwargs)
        self.coefs = self.load()
        self.minz, self.maxz = 0, 0

    def calibrateZ(self, Zlevel):
        """
        Plays a point at the given Z level, the user then focuses on the pattern, and the imaging software should send
        a message with the objective Z level.
        This should be called at multiple different Z levels
        """

        npoints = 7
        l = np.linspace(0, np.pi, npoints)
        xl = 20 * np.cos(l)
        yl = 20 * np.sin(l)
        rl = (5,) * npoints
        zsvg = svg_util.generate_circles_svg(xl, yl, rl)
        self.grab_svg(zsvg, Zlevel)

        key = -1
        while key == -1:
            winname = 'Press any key when in focus'
            cv2.imshow(winname, self.camerahandle.grab_image())
            key = cv2.waitKey(20)
        cv2.destroyWindow(winname)

        self.Zlevels.append(Zlevel / Zscalefactor)

    def setobjectiveZlevel(self, Zobj):
        """Stores the Zlevel of the objective, to be called right after calibrate Z"""
        self.Zobjs.append(Zobj)

    def run(self):
        # have Zlevels which is lens positions
        # and Zobjs which is true objective positions

        Zlevels = np.asarray(self.Zlevels)
        Zobjs = np.asarray(self.Zobjs)
        coefs = np.polyfit(Zobjs, Zlevels, deg=5)
        zmin = Zobjs.min() - .1 * (Zobjs.max() - Zobjs.min())
        zmax = Zobjs.max() + .1 * (Zobjs.max() - Zobjs.min())
        x = np.linspace(zmin, zmax, 500)
        plt.scatter(Zobjs, Zlevels)
        plt.plot(x, np.polyval(coefs, x))
        plt.xlabel("Z obj")
        plt.ylabel("Z level")
        plt.xlim([zmin, zmax])
        plt.ylim([zmin, zmax])
        plt.gca().set_aspect('equal')
        plt.plot(x, x, c='k')

        slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(Zobjs, Zlevels)
        print("linear fit r-value: {0}".format(r_value))
        plt.plot(x, slope * x + intercept, 'g-')

        plt.show()

        self.coefs = coefs
        self.save(self.coefs)

        self.minz = min(Zobjs)
        self.maxz = max(Zobjs)

    def apply(self, frame):
        if self.coefs is not None:
            if self.maxz < frame.Zlevel < self.minz:
                warnings.warn("Trying to play a pattern outside of the Z calibration range")
            frame.Zlevel = np.polyval(self.coefs, frame.Zlevel)
        return frame


from scipy.ndimage import filters
from scipy import fftpack
from pyKriging.krige import kriging


def normalize_img(img):
    """
    :param img: 2D array of an image
    :return: grey level of each pixel normalized between [0,255]
    """
    img = img / np.max(img) * (2 ** 8 - 1)
    img = np.clip(img, 0, 2 ** 8 - 1)
    img = img.astype('uint8')
    return img


class AutoFocus(Calibrator):
    """
    get images and find x,y,z
    focus is defined as the z level with minimum DCT peak, fitted by Kriging
    """

    def __init__(self, *args, **kwargs):
        # if z_pts < 3:
        #     print('at least 3 points are needed')
        self.z_pts = 5
        # getimage=self.camerahandle.grab_image()
        # self.getimage = getimage
        # if startz > endz:  # startz always < endz
        #     startz, endz = endz, startz
        # self.startz = startz
        # self.endz = endz
        self.circle = []
        self.blur_radius = 30
        self.search_z = []
        self.search_interval = []
        self.dct_peaks = []
        self.z_counter = 0
        super(AutoFocus, self).__init__(*args, **kwargs)

    def find_center(self, img, show=False):
        """
        :param z: z-level
        :return: [x,y] coordinate of the beam from the image captured at that z-level
        """
        # img=self.getimage(z)
        blur_img = filters.gaussian_filter(img, (self.blur_radius, self.blur_radius))
        centroid_int = np.unravel_index(blur_img.argmax(), blur_img.shape)
        if show:
            plt.imshow(img)
            plt.scatter(centroid_int[1], centroid_int[0], c='r', s=8)
        return [centroid_int[1], centroid_int[0]]

    def find_sharpness_dct(self, img):
        """
        :param z: z-level
        :return: DCT peak of the beam from the image captured at that z-level
        """
        dct_img = fftpack.dct(filters.gaussian_filter(img, (self.blur_radius, self.blur_radius)))
        return np.max(abs(dct_img))

    # def find_dct_xy(self, z):

    # """
    # :param z: z-level
    # :return: DCT peak and [x,y] coordinate of the beam from the image captured at that z-level
    # """
    # return np.insert(self.find_center(z), 0, self.find_sharpness_dct(z))

    def scan_z_levels(self, holo_z, obj_z, z_pts=3, search_range=100):
        # initialize
        if self.search_z == []:
            self.search_z = np.linspace(obj_z + search_range / 2, obj_z - search_range / 2, num=z_pts)
            self.dct_peaks = []
            self.holo_z = holo_z
            new_z = self.search_z[0]

        elif np.shape(self.dct_peaks)[0] < np.shape(self.search_z)[0]:  # haven't scan all z-levels
            # print(self.search_z)
            # print(self.dct_peaks)
            img = self.grab_image()

            self.dct_peaks.append(self.find_sharpness_dct(img))
            scipy.misc.imsave(str(int(obj_z)) + ' ' + str(int(self.dct_peaks[-1])) + ' img.jpg', img)
            if np.shape(self.dct_peaks)[0] < np.shape(self.search_z)[0]:
                new_z = self.search_z[np.shape(self.dct_peaks)[0]]
            else:
                self.dct_peaks = np.array(self.dct_peaks)
                # do while loop: update start and end until a local minimum is captured
                # by polyfit and re-sample around the fitted focus
                # print(self.search_z)
                # print(self.dct_peaks)
                if all(np.diff(self.dct_peaks) > 0) or all(np.diff(self.dct_peaks) < 0):
                    print('focus maybe out of range')
                    print(self.search_z)
                    print(self.dct_peaks)
                    fit = np.polyfit(self.search_z, self.dct_peaks, 2)
                    print(fit)
                    polyfit_z_focus = -fit[1] / (2 * fit[0])
                    print('polyfit_z_focus: '+str(polyfit_z_focus))
                    # self.search_z = [polyfit_z_focus - self.search_interval, polyfit_z_focus, polyfit_z_focus + self.search_interval]
                    # self.search_z = self.search_z - (self.startz + self.endz) / 2 + polyfit_z_focus
                    self.search_z = np.linspace(polyfit_z_focus + search_range / 2, polyfit_z_focus - search_range / 2, num=z_pts)
                    # self.dct_peaks = [*[self.find_sharpness_dct(z) for z in self.search_z]]
                    self.dct_peaks = []
                    new_z = self.search_z[0]
                else:
                    new_z = 0
                    print('focus predicted by kriging: '+str(self.kriging_dct_focus()))  # use krigning to find focus
        print(self.search_z)
        print(self.dct_peaks)
        return new_z  # 0 means done focusing

    def kriging_dct_focus(self, z_pts=3, thetamin=1e-5, thetamax=1e-2, show=False):
        """
        There are 2 main steps in this function.
        1: Auto-adjust the search range until there is a local minimum within the search range, while preserving the search range
        2: Estimate the x,y,z coord of the focus of the beam
            2.1: Build Kriging model for DCT, x and y coords as a function of z-level
            2.2: Estimate the z-level of focus by taking the minimum of DCT
            2.3: Predict the x,y values of that z-level
        :param z_pts: number of z-levels imaged to determine the focus (by Kriging)
        :param thetamin: lower bound of theta to build the Kriging model, don't need to change usually
        :param thetamax: upper bound of theta to build the Kriging model, should be increased when z_pts increases,
                         meaning less restrained model to prevent over-fitting
        :param show: plot DCT, x and y (and its Kriging fitting and predicted focus point) as a function of z-level
        :return: the coord x,y,z
        """

        # build model
        z_pred = np.linspace(self.search_z[0], self.search_z[-1], num=1000)
        z_pred_2d = np.hstack((np.vstack(z_pred), np.vstack(z_pred)))  # make a 2D input for prediction
        search_z_2d = np.hstack(
            (np.vstack(self.search_z), np.vstack(self.search_z)))  # make a 2D input for building mdoel
        sharpnesses = np.zeros(len(self.search_z), dtype=np.float)
        # for i, z in enumerate(self.search_z):
        #     sharpnesses[i] = self.find_sharpness_dct(z)

        # Use DCT sharpness to find the most in focus Z plane
        krig_dct = kriging(search_z_2d, self.dct_peaks, testPoints=25, thetamin=thetamin, pmin=2,
                           pmax=2)  # TODO change var name!
        krig_dct.thetamax = thetamax
        krig_dct.train(optimizer='ga')
        dct_predict = np.zeros(len(z_pred), dtype=np.float)
        for i, pt in enumerate(z_pred_2d):
            dct_predict[i] = krig_dct.predict(pt)
        focus_arg = dct_predict.argmin()

        # overlay sampling points, prediction curve and focus point
        if show:
            plt.figure(figsize=(5, 5))
            plt.scatter(z_pred, dct_predict, c='r', s=2, label='fit by kriging')
            plt.scatter(z_pred[focus_arg], dct_predict[focus_arg], c='g', s=64, label='focus point')
            plt.scatter(self.search_z, self.dct_peaks, c='b', s=64, label='sampling points')
            plt.title('DCT peak vs z')
            plt.legend()
            plt.show()

        # take an image at that Z plane, and find the XY center
        # return np.append(self.find_center(z_pred[focus_arg]), z_pred[focus_arg])
        return z_pred[focus_arg]
