import numpy as np
from GSF_3D import GS_3D
from matplotlib import pyplot as plt
from matplotlib import gridspec

target_im = np.zeros((256, 256))
target_im[120:130, 130:140] = 255
res = GS_3D([target_im], [0, ])

gs = gridspec.GridSpec(1, 3)
plt.subplot(gs[0])
plt.imshow(target_im)
plt.title('Desired amplitude')

plt.subplot(gs[1])
plt.imshow(res.target_fields[0])
plt.title('Calculated amplitude')

plt.subplot(gs[2])
plt.imshow(res.phase)
plt.title('Calculated phase')

plt.show()
