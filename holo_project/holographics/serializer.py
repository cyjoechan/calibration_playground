from holographics.frame import Frame


def serialize(cmd, frameimages=None):
    """ Takes in a ProtoBuf cmd, and a list of svg images, and returns a list
    that can be fed into ZMQ send_multipart
    """
    msgframes = [cmd.SerializeToString()]

    if frameimages:
        for svg in frameimages:
            msgframes += [svg.encode('utf-8')]
    return msgframes


def unserialize(msg, cmdtype):
    """Reverse of the serialization process, takes in a multipart msg from ZMQ, and
    a ProtoBuf cmdtype, and returns the cmd and a list of frames 
    """
    cmdtype.ParseFromString(msg.pop(0))

    framelist = []
    for image_meta in cmdtype.image_meta:
        frame = Frame(Zlevel=image_meta.Zlevel, frame_num=image_meta.frame_num, duration=image_meta.duration)
        framelist += [frame]

    assert len(framelist) == len(msg)

    for i, frame in enumerate(msg):
        framelist[i].svg = frame

    return cmdtype, framelist
