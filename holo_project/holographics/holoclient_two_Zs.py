import zmq as zmq
# import numpy as np
import time
from holographics import holoclient
from holographics.frame import Frame
from holographics.svg_util import generate_circle_svg, generate_circles_svg


@holoclient.holodec
def twodots(xs, ys, diameters, zs, durations, blank_duration=2.0, power_fraction=.5, corr_factor=.66):
    """
    generates two dots in a sequence with a blank frame between
    power_fraction is the fraction of the total power the first roi gets
    """
    assert 0 <= power_fraction <= 1
    power_fraction2 = (1-power_fraction)

    if power_fraction > power_fraction2:
        hexpower1 = "%.2X" % 255
        hexpower2 = "%.2X" % ((255*power_fraction2)/power_fraction)
    else:
        hexpower1 = "%.2X" % ((255*power_fraction)/power_fraction2)
        hexpower2 = "%.2X" % 255
    hexpowers = [hexpower1, hexpower2]
    print("hexpowers: ", hexpowers)

    frames = []
    frame_num = 0
    for i in range(len(xs)):
        frames.append(Frame(svg=str(generate_circle_svg(xs[i], ys[i], diameters[i], colorhex=hexpowers[i])), Zlevel=zs[i], frame_num=frame_num,
                            duration=durations[i]))
        frame_num += 1
        frames.append(Frame.blankframe(frame_num=frame_num, duration=blank_duration))
        frame_num += 1

    requests = [holoclient.Generate(frames, 960, correction_factor=corr_factor)]
    # requests += [holoclient.Play()]
    return requests


@holoclient.holodec
def twodots_simul(xs, ys, diameters, zs, duration=4, blank_duration=2.0, power_fraction=.5, corr_factor=.66):
    """
    generates two dots in a sequence with a blank frame between
    power_fraction is the fraction of the total power the first roi gets
    """
    assert 0 <= power_fraction <= 1
    power_fraction2 = (1-power_fraction)

    if power_fraction > power_fraction2:
        hexpower1 = "%.2X" % 255
        hexpower2 = "%.2X" % ((255*power_fraction2)/power_fraction)
    else:
        hexpower1 = "%.2X" % ((255*power_fraction)/power_fraction2)
        hexpower2 = "%.2X" % 255
    hexpowers = [hexpower1, hexpower2]
    print("hexpowers: ", hexpowers)

    frames = []

    frames.append(Frame.blankframe(frame_num=0, duration=0.))

    i=0
    frames.append(Frame(svg=str(generate_circle_svg(xs[i], ys[i], diameters[i], colorhex=hexpowers[i])), Zlevel=zs[i], frame_num=1,
                            duration=duration))
    i=1
    frames.append(Frame(svg=str(generate_circle_svg(xs[i], ys[i], diameters[i], colorhex=hexpowers[i])), Zlevel=zs[i], frame_num=1,
                            duration=duration))

    #frames.append(Frame.blankframe(frame_num=2, duration=blank_duration))


    requests = [holoclient.Generate(frames, 960, correction_factor=corr_factor)]
    requests += [holoclient.Play()]
    return requests

@holoclient.holodec
def play():
    return [holoclient.Play()]


@holoclient.holodec
def multiZ(svgpaths, Zs):
    # frames = [Frame(svg=str(generate_circle_svg(-11, -7, 7)), Zlevel=0, frame_num=0, duration=6.0)]
    #frames.append(Frame(svg=str(generate_circle_svg(30, 30, 15)), Zlevel=10, frame_num=0, duration=6.0))
    #frames.append(Frame.blankframe(frame_num=1, duration=4))
    # frames.append(Frame(svg=str(generate_circle_svg(30, -30, 15)), Zlevel=2, frame_num=0, duration=6.0))
    # frames.append(Frame(svg=str(generate_circle_svg(-30, -30, 15)), Zlevel=4, frame_num=0, duration=6.0))
    frames = [Frame.from_svg_path(svgpath, Zlevel=z, frame_num=0, duration=5.0) for svgpath, z in zip(svgpaths, Zs)]

    requests = []
    requests += [holoclient.Generate(frames, 920, correction_factor=.81)]
    requests += [holoclient.Play()]

    return requests


if __name__ == '__main__':
    # filepath1 = 'sample_images/nato.svg'
    filepath0 = 'sample_images/gradient.svg'
    filepath1 = 'sample_images/minerva.svg'
    filepath2 ='sample_images/mpi.svg'

    # f =Frame.from_svg_path(filepath2, Zlevel=0, frame_num=0, duration=5.0)
    # f.rasterize()
    # from matplotlib import pyplot as plt
    # plt.imshow(f.raster); plt.show()

    # multiZ([filepath1, filepath2], [-10, 15])
    multiZ([filepath0], [0])

    # multiZ('', [0])

    #
    # xs = [-20, 30]
    # ys = [30, 20]
    # diameters = [15, 10]
    # zs = [10, -10]
    # durations = [6, 6]
    # # power_fraction = .75
    # # for frac in [.2, .4, .6, .8]:
    # #     pass
    # #     twodots_simul(xs, ys, diameters, zs, 4, power_fraction=frac, corr_factor=.75, blank_duration=0)
    # twodots_simul(xs, ys, diameters, zs, 10, power_fraction=1, corr_factor=.76, blank_duration=0)
    # #play()


    # from PIL import Image
    # import numpy as np
    # from matplotlib import pyplot as plt
    # img1 = np.asarray(Image.open(filepath1))
    # plt.imshow(img1, 'gray'); plt.show()
    # f = Frame(raster=img1)
    # f.generate(920)
