from __future__ import print_function, division

from holographics.holoclient import *


class Holoclient(object):
    def __init__(self, address="tcp://localhost:51233"):
        print("Connecting to holo server at %s" % address)
        context = zmq.Context()
        self.socket = context.socket(zmq.REQ)
        self.socket.connect(address)

    def status(self):
        return Status().send(self.socket)

    def play(self):
        return Play().send(self.socket)

    def generate(self, *args, **kwargs):
        return Generate(*args, **kwargs).send(self.socket)
