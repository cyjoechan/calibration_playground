import zmq as zmq
import numpy as np
import time
from holographics import holoclient
from holographics.frame import Frame
from holographics.svg_util import generate_circle_svg, generate_circles_svg


@holoclient.holodec
def test():
    frames = []

    distance = 40
    rs = [10, 5, 20, 5, 10, 15]
    l = np.linspace(0, 4*np.pi, 6, endpoint=False)
    xs, ys = np.linspace(-70, 70, 6), [30,]*6 #distance*np.sin(l)
    zlevels = np.linspace(-20, 20, 6)
    for x, y,z, r  in zip(xs, ys, zlevels, rs):
        frames.append(Frame(svg=str(generate_circles_svg([x], [y], [r])), Zlevel=z, frame_num=0, duration=6.0))
    # frames.append(Frame(svg=str(generate_circles_svg([10, 30, 50], [10, 30, 50], [10,10,10])), Zlevel=0, frame_num=0, duration=6.0))
    # frames.append(Frame(svg=str(generate_circles_svg([-20, -40], [20, 40], [10, 10])), Zlevel=40, frame_num=0,
    #                     duration=6.0))

    # frames.append(Frame(svg=str(generate_circles_svg([10, 30, 50], [10, 30, 50], [10,10,10])), Zlevel=0, frame_num=0, duration=6.0))
    # frames.append(Frame(svg=str(generate_circles_svg(np.linspace(-50, 50, 10), [12,]*10, [2.5,]*10)), Zlevel=0, frame_num=0,
    #                     duration=16.0))

    # l = np.linspace(10, 60, 6)
    # x, y = np.meshgrid(np.linspace(20, 100, 4), np.linspace(-80, 80, 6))
    # x = x.ravel()
    # y = y.ravel()
    # # c = ["ff",]*8 + ["77",]*8 + ["ff",]*8 + ["ff",]*8
    # c = ["ff"] * len(x)
    # # for i in range(len(c)):
    # #     if i % 4 == 1:
    # #         c[i] = "80"
    # frames.append(
    #     Frame(svg=str(generate_circles_svg(x, y, [7, ] * len(x), colorhexes=c)), Zlevel=10, frame_num=0,
    #           duration=16.0))
    #
    # # print x
    # frames[0].rasterize()
    # from matplotlib import pyplot as plt
    # plt.imshow(frames[0].raster, cmap=plt.cm.Greys_r)
    # plt.colorbar()
    # plt.show()


    # frames.append(Frame(svg=str(generate_circle_svg(30, 0, 10, colorhex="ef")), Zlevel=5, frame_num=0, duration=6.0))
    # frames.append(Frame(svg=str(generate_circle_svg(0, 30, 10, colorhex="ff")), Zlevel=5, frame_num=0, duration=6.0))
    # frames.append(Frame(svg=str(generate_circle_svg(-30, -30, 10, colorhex="ff")), Zlevel=5, frame_num=0, duration=6.0))
    # frames.append(Frame(svg=str(generate_circle_svg(70, 70, 10)), Zlevel=0, frame_num=0, duration=6.0))
    # frames.append(Frame(svg=str(generate_circle_svg(20, 10, 5)), Zlevel=0, frame_num=0, duration=6.0))
    requests = []
    requests += [holoclient.Generate(frames, 920, correction_factor=.83)]
    requests += [holoclient.Play()]
    return requests


print(test())
