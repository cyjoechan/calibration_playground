
class LimitedAttribute(object):
    def __init__(self, value=None, minvalue=None, maxvalue=None, valuetype=None,):
        if valuetype is None:
            self.valuetype = type(value)

        self.minvalue = minvalue
        self.maxvalue = maxvalue

        self.__set__(self, value)

    def __get__(self, instance, owner):
        # print "in get"
        return self.value

    def __set__(self, instance, value):
        if self.minvalue and value < self.minvalue:
            raise ValueError("Tried to set below maximum value")
        if self.maxvalue and value > self.maxvalue:
            raise ValueError("Tried to set above maximum value")

        self.value = value
#TODO, add type enforcement (optionally?)


class GSF(object):
    numiterations = LimitedAttribute(2, 1, 50)

    def __init__(self):
        pass
        #TODO add convince introspection setting of limitedattribs?

    @property
    def settable_attributes(self):
        return [k for k, v in type(self).__dict__.iteritems() if isinstance(v, LimitedAttribute)]

    def __call__(self, target, Zoffset = 0, *args, **kwargs):
        holo = self.GSF(target, *args, **kwargs)
        if Zoffset != 0:
            return self.applyLens(holo, Zoffset)
        return holo

    def GSF(self, *args, **kwargs):
        print(self.numiterations)
        return 'computed hologram: %s' % args[0]

    def applyLens(self, pattern, Zoffset):
        return 'somelens applied: %s' % pattern


if __name__ == '__main__':
    gsf = GSF()
    print(gsf.settable_attributes)

    print(gsf.numiterations)
    gsf.numiterations = 5
    print(gsf.numiterations)
    # gsf.numiterations = 70
    print(gsf.numiterations)

    print(gsf('someholodata1'))
    print(gsf('someholodata2', 5))

