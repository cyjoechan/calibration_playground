from holographics.calibration2 import ZCalibrator, Frame, np


def test_ZCalibrator():
    z = ZCalibrator(None, None)
    import svg_util
    import matplotlib.pyplot as plt


    f = Frame(svg=svg_util.generate_circle_svg(30, 30, 15))
    f.set_svg_bounds()
    f.rasterize()
    # plt.imshow(f.raster, 'gray')
    # plt.show()

    center = z.findcenter(f.raster)
    print center
    print z.findcenter2(f.raster)

    print z.findradius(f.raster)
    print z.findradius2(f.raster)


if __name__ == '__main__':
    test_ZCalibrator()
