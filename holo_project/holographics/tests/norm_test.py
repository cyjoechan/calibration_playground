import numpy as np
from matplotlib import pyplot as plt

arr0 = np.zeros((10, 10))
arr0[4:, :] = 3
print (arr0)

arr1 = np.zeros((10, 10))
arr1[4:, :] = 1
arr1[1:, :] = .7


def norm(arr0, arr1):
    #TODO handle if one array is blank
    arr0 = arr0.ravel()
    arr1 = arr1.ravel()
    return np.dot(arr0, arr1) / ((arr0 ** 2).sum() ** .5 * (arr1 ** 2).sum() ** .5)


# res = np.dot(arr0, arr1)/((arr0**2).sum()**.5*(arr1**2).sum()**.5)
res = norm(arr0, arr1)
print (res)

plt.plot(arr0)
plt.plot(arr1)
# plt.plot(res)
plt.show()
