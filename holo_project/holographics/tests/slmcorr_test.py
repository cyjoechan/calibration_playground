from __future__ import print_function, division
from holographics.SLM_correction import SLM_correction
import numpy as np
import holographics.playframes
from holographics.frame import Frame

if __name__ == "__main__":
    dims = (792, 600)
    f = holographics.playframes.Frameplayer(*dims)

    from skimage import io

    patt = np.rollaxis(io.imread('sample_hologram_hpk.bmp'), 0, 2)[::-1, ::-1]
    print(patt.mean())
    # patt = (patt*0.6).astype('uint8')
    # patt = (patt*(148/255)).astype('uint8')
    print(patt.mean())

    frame = Frame(holograms=[patt], Zlevel=0, frame_num=0, duration=0.0)
    frame1 = Frame(holograms=[patt.copy()], Zlevel=0, frame_num=1, duration=1.0)
    frames = [frame, frame1]
    # corrs = np.arange(.68, .71, .01)
    # print corrs

    slmcorr = SLM_correction()
    wavelength = 760
    [frame.apply_deformation_correction(slmcorr, wavelength) for frame in frames]
    # print [frame.hologram.mean() for frame in frames]
    [frame.apply_LUT_correction(slmcorr, wavelength, correction_factor=1.0) for frame in frames]
    # print [frame.hologram.mean() for frame in frames]

    f.loadframes(frames)
    print('playing')
    f.playframes()
    print('done')
    input('press to exit')
