from __future__ import print_function
from holographics.frame import Frame
from matplotlib import pyplot as plt
from holographics.GSF import GS_new, GS_FFT


if __name__ == '__main__':
    frame = Frame.from_svg_path(r'../sample_images/logo.svg', Zlevel=2.5, frame_num=0)
    frame.rasterize()
    print(frame)

    res = GS_FFT(frame.raster, iterations=10)

    # plt.imshow(frame.raster)
    plt.imshow(res.phase)
    plt.show()


    # frame.generate()
    # from SLM_correction import SLM_correction
    # s = SLM_correction()
    # print frame.hologram.shape
    # frame.apply_deformation_correction(s, 920)
    # frame.apply_LUT_correction(s, 920)
    #
    # print frame.hologram.shape
    #
    # frame2 = Frame.from_raster_path('sample_hologram_hpk.bmp', Zlevel=2.5, frame_num=0, frame_duration=3)
    # print frame2.raster
    # frame2.generate()

    # f = np.zeros((600, 792)).astype('uint8')
    # f[330:360, 375:425] = 255
    # frame = Frame(raster=f, frame_duration=60*20)
    # frame.generate()

