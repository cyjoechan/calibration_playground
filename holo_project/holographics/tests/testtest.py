import numpy as np
from matplotlib import pyplot as plt
import GSF
from skimage import io
from PIL import Image
# import Image

# amp = np.zeros((512,512))
# amp[260:300, 230:450] = 1

with open('test.png', 'rb') as f:
    amp = np.array(Image.open(f))[..., 0]

# plt.imshow(res.phase)
# plt.show()

res = GSF.GS_FFT(amp, iterations=15)

im = res.phase % (2 * np.pi)

im -= im.min()
im = im/im.max()

# im = Image.fromarray(im).convert("L")
im = Image.fromarray(np.uint8(im*200), 'L')

im.save('test_phaseout_200.bmp')
# io.imsave('test_phaseout.bmp', im)
