from __future__ import print_function
import numpy as np
from holographics.playframes import Frameplayer
from holographics.frame import Frame
import holographics.frame_computation


def random_tex(dimensions):
    data = np.random.random_integers(low=0, high=255, size=(dimensions[0], dimensions[1])).astype('uint8')
    return data


def lin_tex(dimensions, index=0):
    data = np.zeros(dimensions)
    for i in range(data.shape[index]):
        if index == 0:
            data[i, :] = i % 256
        else:
            data[:, i] = i % 256
    return data.astype('uint8')


if __name__ == "__main__":
    dims = (792, 600)
    f = Frameplayer(*dims, fullscreen='auto')

    from holographics.svg_util import generate_circle_svg

    # frames = [Frame(holograms=[random_tex(dims)], Zlevel=0, frame_num=i, duration=i) for i in range(1, 4)]
    frames = [Frame(svg=str(generate_circle_svg(0, 60, 8)), Zlevel=0, frame_num=0, duration=1.0)]
    frames.append(Frame(svg=str(generate_circle_svg(0, 60, 12)), Zlevel=0, frame_num=1, duration=1.0))
    frames.append(Frame(svg=str(generate_circle_svg(0, 60, 20)), Zlevel=0, frame_num=1, duration=1.0))
    for frame in frames:
        frame.rasterize()

    holographics.frame_computation.simple_generate_frames(frames)
    print([frame.holograms for frame in frames])

    # frames = [Frame(hologram=lin_tex(dims), Zlevel=0, frame_num=0, duration=1.5)]
    # frames += [Frame(hologram=lin_tex(dims, 1), Zlevel=0, frame_num=1, duration=3.0)]
    # from skimage import io
    #
    # patt = np.rollaxis(io.imread('sample_hologram_hpk.bmp'), 0, 2)[::-1, ::-1]
    # patt = (patt * (148 / 255)).astype('uint8')
    # frame = Frame(hologram=patt, Zlevel=0, frame_num=1, duration=1.0)
    # frames = [frame, frame]
    # frames += [Frame(hologram=lin_tex(dims, 1), Zlevel=0, frame_num=2, duration=4.0)]
    f.loadframes(frames)
    print('playing')
    f.playframes()
    print('done')