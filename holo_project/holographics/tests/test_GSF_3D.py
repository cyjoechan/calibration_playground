import numpy as np
pi = np.pi
import matplotlib.pyplot as plt
from PIL import Image
from holographics.GSF_3D import GS_3D
import unittest


class TestGSF(unittest.TestCase):
    def testGSF(self):
        im = Image.open('sample_images/004_mask.png')
        scale = 1
        im = im.resize((im.size[1] / scale, im.size[0] / scale), Image.NEAREST)
        x = np.array(im.getdata())
        target_amplitude = np.reshape(x, (im.size[1], im.size[0]))

        Zshifts = np.linspace(0, .5, 2)
        gs = plt.GridSpec(3, len(Zshifts)+1, wspace=.02, hspace=.02)
        plt.figure()

        for i, Z in enumerate(Zshifts):
            res = GS_3D([target_amplitude, target_amplitude.copy()[::-1, :]], [Z, Z+.3], iterations=20)
            # out = GS_3D([target_amplitude], [Z], iterations=20)
            self.assertAlmostEqual(res.phase.min(), 0, places=3)
            self.assertAlmostEqual(res.phase.max(), 2 * pi, places=3)
            self.assertEqual(target_amplitude.shape, res.phase.shape)

            plt.subplot(gs[0, i])
            plt.title("Z= %.2f" % Z)
            plt.imshow(res.phase, 'gray')
            # plt.colorbar()

            plt.subplot(gs[1, i])
            plt.imshow(res.target_fields[0], 'gray')
            plt.colorbar()

            plt.subplot(gs[2, i])
            plt.imshow(res.target_fields[0], 'gray')
            plt.colorbar()

            # plt.subplot(gs[3, i+1])
            # plt.plot(out[2])

        plt.show()


if __name__ == '__main__':
    t = TestGSF()
    t.run()
    plt.show()
