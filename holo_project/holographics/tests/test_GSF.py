import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from holographics.GSF import GSF_Zlens
import unittest

pi = np.pi


class TestGSF(unittest.TestCase):
    def testGSF(self):
        im = Image.open('sample_images/004_mask.png')
        scale = 1
        im = im.resize((im.size[1] / scale, im.size[0] / scale), Image.NEAREST)
        x = np.array(im.getdata())
        target_amplitude = np.reshape(x, (im.size[1], im.size[0]))

        Zshifts = np.linspace(0, 1, 2)
        gs = plt.GridSpec(4, len(Zshifts), wspace=.02, hspace=.02)
        plt.figure()
        for i, Z in enumerate(Zshifts):
            res = GSF_Zlens(target_amplitude, Z=Z, wavelength=960, iterations=20)
            self.assertAlmostEqual(res.phase.min(), 0, places=3)
            self.assertAlmostEqual(res.phase.max(), 2 * pi, places=3)
            self.assertEqual(target_amplitude.shape, res.phase.shape)
            # import time
            # t0 = time.time()
            # nreps =30
            # [appplylens(out[1], Z) for j in range(0, nreps)]
            # print (time.time() - t0)/nreps

            plt.subplot(gs[0, i])
            plt.title("Z= %.2f" % Z)
            plt.imshow(res.phase, 'gray')
            # plt.colorbar()

            plt.subplot(gs[1, i])
            plt.imshow(res.target_fields[0], 'gray')
            plt.colorbar()

            if res.errors:
                plt.subplot(gs[2, i])
                plt.plot(res.errors[2])

            if res.lenses:
                plt.subplot(gs[3, i])
                plt.imshow(res.lenses[0], 'gray')
                # print out[2].min(), out[2].max(), 'lens'
                # plt.colorbar()
                # print '.',
        plt.show()

    @unittest.skip("Skip, visual output")
    def show(self):
        plt.show()


# if __name__ == '__main__':
#     t = TestGSF()

