import numpy as np
from scipy import optimize
from holographics.transformations import *

testarray = np.asarray([[-40, -40], [40, -40], [40, 40], [-40, 40]])
# testarray2 = np.asarray([[-40, -40], [40, -40], [40, 40], [-40, 40]])
testarray2 = np.asarray([[-40.1, -40.1], [40, -40.1], [40.1, 39.9], [-40.1, 39.9]])
testarray2[:, 0] += 5
# print testarray, testarray2


def test_pad_trans():
    eye = np.identity(3)
    assert (pad_trans(eye[:2, :]) == eye).all()


def test_fit_trans():
    trans, err = fit_trans(testarray, testarray2)
    assert reprojection_error(testarray, testarray2, trans) < .1



