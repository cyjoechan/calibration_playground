from holographics.SLM_correction import *


if __name__ == "__main__":
    s = SLM_correction()
    import pylab

    l = np.linspace(600, 1150, 2000)
    # pylab.plot(l, 293/255*255/s.wavelength_LUT(l))
    # pylab.show()
    l = np.arange(600, 1150, 10)
    for i in l:
        # print i, '  ', (255/293.)*255./s.wavelength_LUT(i)
        print(i, ' ', s.get_LUT_correction(i) * 255)
        # print s.get_deformation_pattern(950).shape
