from __future__ import print_function
import cv2
import time
print(cv2.__version__)
from holographics.calibration2 import XimeaCameraHandle, cvCameraHandle

# class CameraTest(object):
#     def __init__(self, video_src=0, debug=False):
#         self.cam = cv2.VideoCapture(video_src)
#         # self.cam.set(cv2.CV_CAP_PROP_FRAME_WIDTH, 1280)
#         # self.cam.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, 1024)
#         #
#         # print(self.cam.get(cv2.cv.CV_CAP_PROP_GAIN))
#         # self.cam.set(cv2.cv.CV_CAP_PROP_EXPOSURE, 2.0)
#         # print(self.cam.get(cv2.cv.CV_CAP_PROP_GAIN))
#
#         assert self.cam.isOpened(), "Camera failed to start"
#
#         ret, self.frame = self.cam.read()
#         print('Return value', ret)
#         print(self.frame.shape)
#         cv2.namedWindow('holo_calibration')
#
#     def run(self):
#         self.running = True
#         while self.running:
#             previous_time = time.time()
#             ret, self.frame = self.cam.read()
#             # print("Est. inst. FPS: %0.2f " % (1 / (time.time() - previous_time)))
#
#             vis = self.frame.copy()
#             cv2.imshow('holo_calibration', vis)
#             key = cv2.waitKey(1)
#             if key > 0 and chr(key) in ['q', 'Q'] or key == 27:
#                 self.running = False


if __name__ == '__main__':
    # h = CameraTest(1)
    # h.run()
    # cv2.destroyAllWindows()
    cam = XimeaCameraHandle()
    img = cam.grab_image()
    cam.release_cam()
    print(img.shape)
    from matplotlib import pyplot as plt
    plt.imshow(img)
    plt.show()
