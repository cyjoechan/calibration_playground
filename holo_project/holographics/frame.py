import copy

import numpy as np
from PIL import Image

from holographics import svg_util
from holographics.svg_util import add_background, svg_to_np, set_svg_bounds_centered

compute_size = (792, 792)
target_size = (792, 600)
svg_target_size_x = 384  # in um for target field size
svg_target_size_y = int(svg_target_size_x * float(compute_size[0]) / compute_size[1])


class Frame(object):
    def __init__(self, svg=None, raster=None, holograms=None, Zlevel=0, frame_num=None, duration=0):
        self.svg = svg
        self.raster = raster
        self.Zlevel = Zlevel
        self.frame_num = frame_num
        self.duration = duration
        self.holograms = holograms
        self.computedpattern = None

    def rasterize(self):
        assert self.svg
        self.set_svg_bounds()

        dpi = (72 * svg_target_size_x / float(compute_size[0]))
        raster = svg_to_np(self.svg, dpi)
        assert np.diff(raster[:, :, :3]).sum() == 0, 'All svg color channels should be the same'
        # there's some rounding issues with rasterization depending on dpi, poking in cairo didn't get far yet
        assert compute_size == raster.shape[:-1]

        self.raster = raster[:, :, 0]

    def apply_deformation_correction(self, SLM_correction, *args, **kwargs):
        self.holograms = [SLM_correction.apply_deformation_pattern(holo, *args, **kwargs) for holo in self.holograms]

    def apply_LUT_correction(self, SLM_correction, *args, **kwargs):
        self.holograms = [SLM_correction.apply_LUT(holo, *args, **kwargs) for holo in self.holograms]
        # TODO add a flag if it's already been calibrated or not

    def apply_factor_correction(self, factor):
        self.holograms = [(holo * factor).astype('uint8') for holo in self.holograms]

    @classmethod
    def from_svg_path(self, svgpath, *args, **kwargs):
        with open(svgpath) as f:
            svg = f.read()
        return self(svg, *args, **kwargs)

    @classmethod
    def from_raster_path(self, rasterpath, *args, **kwargs):
        raster = np.asarray(Image.open(rasterpath))
        return self(raster=raster, *args, **kwargs)

    @classmethod
    def blankframe(cls, *args, **kwargs):
        svg = svg_util.empty_svg()
        raster = np.zeros(target_size)
        return cls(svg=svg, raster=raster, *args, **kwargs)

    def copy(self):
        return copy.deepcopy(self)

    def set_svg_bounds(self):
        self.svg = add_background(
            set_svg_bounds_centered(self.svg, svg_target_size_x, svg_target_size_y))
