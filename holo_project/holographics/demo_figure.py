from __future__ import print_function
import numpy as np
import time

from functools import wraps
from matplotlib import pyplot as plt
from skimage import io
from holographics import holoclient
from holographics.frame import Frame
from holographics.svg_util import generate_circle_svg, generate_circles_svg


@holoclient.holodec
def playframes(frames):
    requests = []
    requests += [holoclient.Generate(frames, 820, correction_factor=.80)]
    requests += [holoclient.Play()]
    return requests


def power2hexes(powers):
    powers = np.asarray(powers, dtype=np.float)
    powers /= powers.max()
    hexpowers = ["%.2X" % (255 * p) for p in powers]
    return hexpowers


def dotgrid():
    xs, ys = np.meshgrid(np.linspace(30, 90, 3), np.linspace(-80, 80, 6))
    c = np.asarray(["ff"] * xs.size)
    rs = np.asarray([5, ] * xs.size)

    x, y = xs.ravel(), ys.ravel()
    r = rs
    f = Frame(svg=str(generate_circles_svg(x, y, r, colorhexes=c)), Zlevel=0, frame_num=0,
              duration=8.0)

    return [f]


xs, ys = np.meshgrid(np.linspace(-62, 58, 4), [25])
c = np.asarray(["ff"] * xs.size)
rs = np.asarray([12, ] * xs.size)
powers = np.asarray([1, 2, .5, 1])
Zs = [-15, 14]
# Zs = [0, 0]
zidx = np.asarray([1, 0, 1, 0, ], dtype=np.bool)


def dotgridpower():
    x, y = xs.ravel(), ys.ravel()
    r = rs
    c = power2hexes(powers)
    print(powers, c)
    print(len(x), len(powers))
    f = Frame(svg=str(generate_circles_svg(x, y, r, colorhexes=c)), Zlevel=0, frame_num=0,
              duration=8.0)

    f.rasterize()
    io.imsave('output_images/target dotgridpower.png', f.raster)

    # plt.matshow(f.raster, cmap=plt.cm.Greys_r);
    # plt.show()

    return [f]


def dotgridz():
    x, y = xs.ravel(), ys.ravel()
    r = rs

    f0 = Frame(svg=str(generate_circles_svg(x[zidx], y[zidx], r[zidx], colorhexes=c[zidx])), Zlevel=Zs[0], frame_num=0,
               duration=8.0)
    zidxn = np.logical_not(zidx)
    f1 = Frame(svg=str(generate_circles_svg(x[zidxn], y[zidxn], r[zidxn], colorhexes=c[zidxn])), Zlevel=Zs[1],
               frame_num=0,
               duration=8.0)

    f0.rasterize()
    io.imsave('output_images/target dotgridz0.png', f0.raster)
    f1.rasterize()
    io.imsave('output_images/target dotgridz1.png', f1.raster)
    return [f0, f1]


def dotgridzpower():
    x, y = xs.ravel(), ys.ravel()
    r = rs
    c = power2hexes(powers)
    c = np.asarray(c)
    f0 = Frame(svg=str(generate_circles_svg(x[zidx], y[zidx], r[zidx], colorhexes=c[zidx])), Zlevel=Zs[0],
               frame_num=0,
               duration=8.0)
    zidxn = np.logical_not(zidx)
    f1 = Frame(svg=str(generate_circles_svg(x[zidxn], y[zidxn], r[zidxn], colorhexes=c[zidxn])), Zlevel=Zs[1],
               frame_num=0,
               duration=8.0)
    f0.rasterize()
    io.imsave('output_images/target dotgridzpower0.png', f0.raster)
    f1.rasterize()
    io.imsave('output_images/target dotgridzpower1.png', f1.raster)
    return [f0, f1]


# playframes(dotgrid())
# playframes(dotgridpower())
# playframes(dotgridz())
playframes(dotgridzpower())
