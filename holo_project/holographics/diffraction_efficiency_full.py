from __future__ import division
import numpy as np
import scipy.special

Si = lambda x: scipy.special.sici(x)[0]
sinc = np.sinc
pi = np.pi
cos = np.cos


def diffractioneff2d(x, y):
    focal_ratio = .005
    wavelength = .920  # in um
    SLMpixel = 14  # in um
    constants = pi * focal_ratio * SLMpixel / wavelength
    print ("sinc consts:", constants)
    # constants = 1
    efficiency = sinc(constants * x) ** 2 * sinc(constants * y) ** 2
    return efficiency


def integrated(x):
    # integral sinc(x/2)**2 = (2 (-1 + cos(x) + x Si(x)))/x
    with np.errstate(divide='ignore', invalid='ignore'):
        res = (2 * (-1 + cos(x) + x * Si(x))) / x
        res[~ np.isfinite(res)] = 0
    return res


def diffractioneff3d(x, y, zeta):
    # 1 / (4* zeta**2) * integrated(x) | (x+zeta,x-zeta)*integrated(y)|(y+zeta, y-zeta)
    x, y, zeta = np.atleast_2d(np.asarray(x)), np.atleast_2d(np.asarray(y)), np.atleast_2d(np.asarray(zeta))
    inted_x = integrated(x + zeta) - integrated(x - zeta)
    inted_y = integrated(y + zeta) - integrated(y - zeta)
    with np.errstate(divide='ignore', invalid='ignore'):
        res = 1 / np.asarray(4 * zeta ** 2) * inted_x * inted_y
        res[~ np.isfinite(res)] = 1  # TODO fix divide by zero, should reduce to XY case

    # total case should be ((-1 + cos(x) + x Si(x)) (-1 + cos(y) + y Si(y)))/(x y z^2)

    return res


def diff3dnew(x, y, z, xyscale=.006, zscale=.002):
    """
    Smaller number for scale decreases the amount of correction
    """
    return sinc(xyscale * x / 2) ** 2 * sinc(xyscale * y / 2) ** 2 * sinc(zscale * z / 2) ** 2
    #
    # if np.isclose(z, 0, rtol=1e-7):
    #     return sinc(x / 2) ** 2 * sinc(y / 2) ** 2
    # else:
    #     return 1 / (4 * z ** 2) * (sinc((x + z) / 2) ** 2 - sinc((x - z) / 2) ** 2) * (
    #         sinc((y + z) / 2) ** 2 - sinc((y - z) / 2) ** 2)
    #


if __name__ == "__main__":
    from matplotlib import pyplot as plt

    diffs = np.asarray([diff3dnew(d, d, 0.00) for d in [10, 30, 50]])
    diffs /= diffs.max()
    print (diffs)
    # [ 1.        ,  0.75565611,  0.47963801]

    # l = np.linspace(-1, 1, 512)
    l = np.linspace(-150, 150, 512)

    x, y = np.meshgrid(l, l)
    diff = diff3dnew(x, 0, y)
    cs = plt.contour(x, y, diff)#, np.arange(.4, 1, .1))
    plt.clabel(cs, inline=1, fontsize=10)
    plt.show()
