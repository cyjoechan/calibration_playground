from __future__ import division
from __future__ import print_function

import numpy as np
from matplotlib import pyplot as plt, gridspec

from holographics.diffraction_efficiency_full import diffractioneff3d, sinc, pi
from holographics import GSF_3D

scale = .043
# diffs = np.asarray([diffractioneff3d(d * scale, d * scale, 0.0001) for d in [10, 30, 50]])
# diffs /= diffs.max()

# print(diffs)
# Measured brightnesses:
# 10,10 from center - 221
# 30,30 from center - 167
# 50,50 from center - 106
# [ 1.        ,  0.75565611,  0.47963801]


im1 = np.zeros((512, 512))
im1[150:300, 300:350] = 1
# im1[150:200, 100:200] = 1

im2 = np.zeros((512, 512))
im2[300:350, 100:200] = 2
im2[180:200, 250:300] = .5

ims = [im1, im2]
zlevels = [-20, 20]


halfsize = 100
lx = np.linspace(-halfsize * scale, halfsize * scale, 512) #TODO change scale
x, y = np.meshgrid(lx, lx)
diff3d = diffractioneff3d(x, y, .001)

ims_sinced = [im.copy() / diff3d for im in ims]


assert len(ims) == len(zlevels)
nplanes = len(ims)
iters = 25
print("NORMED: ", GSF_3D.normedplanes(ims_sinced, ims_sinced))
res0 = GSF_3D.GS_3D(ims_sinced, zlevels, iterations=iters)

cor = np.asarray(res0.correlations)
if 0:
    plt.plot(cor / cor.sum(1)[:, None])
    [plt.axhline(n,  c='k') for n in GSF_3D.normedplanes(ims_sinced, ims_sinced)]

plt.show()

immax = max([im.max() for im in ims])

pmax = max([np.percentile(im, 99.9) for im in res0.target_fields])

sinced_results = [diff3d * tf for tf in res0.target_fields]
tfmax = max([np.percentile(im, 99.9) for im in sinced_results])

gs = gridspec.GridSpec(nplanes, 3)
for planenum in range(nplanes):
    plt.subplot(gs[planenum, 0])
    plt.imshow(ims[planenum], cmap=plt.cm.Greys_r, vmin=0, vmax=immax, extent=[-halfsize, halfsize, -halfsize, halfsize])
    plt.ylabel("Zlevel {0}".format(zlevels[planenum]))
    if planenum == 0:
        plt.title("Desired pattern")

    plt.subplot(gs[planenum, 1])
    plt.imshow(res0.target_fields[planenum], cmap=plt.cm.Greys_r, vmin=0, vmax=pmax,
               extent=[-halfsize, halfsize, -halfsize, halfsize])  # , vmin=pmin, vmax=pmax)
    if planenum == 0:
        plt.title("Computed pattern")
        # TODO emission profiles **2

    plt.subplot(gs[planenum, 2])
    plt.imshow(sinced_results[planenum], cmap=plt.cm.Greys_r, vmin=0, vmax=tfmax,
               extent=[-halfsize, halfsize, -halfsize, halfsize])  # , vmin=pmin, vmax=pmax)
    # plt.imshow(diff3d, cmap=plt.cm.Greys_r)  # , vmin=pmin, vmax=pmax)
    # plt.imshow(ndimage.interpolation.zoom(res0.target_fields[planenum], 1 / 16), cmap=plt.cm.Greys_r)
    if planenum == 0:
        plt.title("Computed pattern, sinc")
        # plt.colorbar()

    # plt.subplot(gs[planenum, 3])
    # plt.imshow((diff3d * res0.target_fields[planenum])**2, cmap=plt.cm.Greys_r, vmin=0,
    #            extent=[-150, 150, -150, 150])  # , vmin=pmin, vmax=pmax)
    # if planenum == 0:
    #     plt.title("Computed emission pattern, including sinc")
plt.show()


