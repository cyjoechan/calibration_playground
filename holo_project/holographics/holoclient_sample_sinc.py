import zmq as zmq
import numpy as np
import time
from holographics import holoclient
from holographics.frame import Frame
from holographics.svg_util import generate_circle_svg, generate_circles_svg


@holoclient.holodec
def test():
    frames = []

    # l = np.linspace(10, 60, 6)
    x, y = np.meshgrid(np.linspace(-60, 60, 4), [20])
    x = x.ravel()
    y = y.ravel()
    c = ["ff"] * len(x)
    for i in range(len(c)):
        if i % 4 == 1:
            c[i] = "80"
        elif i % 4 == 2:
            c[i] = "40"
        elif i % 4 == 3:
            c[i] = "20"
    frames.append(
        Frame(svg=str(generate_circles_svg(x, y, [7, ] * len(x), colorhexes=c)), Zlevel=10, frame_num=0,
              duration=16.0))
    requests = []
    requests += [holoclient.Generate(frames, 920, correction_factor=.83)]
    requests += [holoclient.Play()]
    return requests


print(test())
