from scipy.stats import gaussian_kde
import numpy as np
from matplotlib import pyplot as plt
from scipy.ndimage import filters
from pyKriging.krige import kriging
from pyKriging.regressionkrige import regression_kriging
from scipy import fftpack

from mic_simulator import generate_points_3d, SimImg


def normalize_img(img):
    """
    :param img: 2D array of an image
    :return: grey level of each pixel normalized between [0,255]
    """
    img = img / np.max(img) * (2 ** 8 - 1)
    img = np.clip(img, 0, 2 ** 8 - 1)
    img = img.astype('uint8')
    return img


def distance_kde(coords1, coords2, show=False):
    """
    :param coords1: 2D array of an image 
    :param coords2: 2D array of another image 
    :param show: plot histogram and KDE if true
    :return: 
    """
    distances = np.sqrt(np.sum((coords1 - coords2) ** 2, 1))
    kernel = gaussian_kde(distances)
    x_kde = np.linspace(0, max(distances), num=1000)
    y_kde = kernel.evaluate(np.linspace(0, max(distances), num=1000)) / kernel.n * kernel._norm_factor
    if show:
        plt.hist(distances, bins='auto')
        plt.xlabel('distance error')
        plt.ylabel('number of points')
        plt.show()
        plt.scatter(x_kde, y_kde, c='b', s=1)
        plt.xlabel('distance error')
        plt.ylabel('density')
        plt.show()
    return x_kde, y_kde


def mesh_3d(n_pts, size_of_img):
    """
    :param n_pts: number of points per axis, its cubic equal total points (2**3->8, 4**3->64)
    :param size_of_img: rectangle in 3D, represented by a 3x2 matrix, row 0:2 is x,y,z, column 0:1 is upper/lower bound
    :return:
    """
    # mesh = []
    # for i in range(3):
    #     mesh.append(np.linspace(size_of_img[i][0], size_of_img[i][1], num=n_pts))
    # mesh = np.meshgrid(mesh[0], mesh[1], mesh[2])

    mesh = np.meshgrid(*[np.linspace(size_of_img[i][0], size_of_img[i][1], num=n_pts) for i in range(3)])
    mesh = np.reshape(np.ravel(mesh), (3, n_pts ** 3)).T
    return mesh


class AutoFocus(object):
    """
    get images and find x,y,z
    focus is defined as the z level with minimum DCT peak, fitted by Kriging
    """

    def __init__(self, getimage, startz, endz):
        self.getimage = getimage
        if startz > endz:  # startz always < endz
            startz, endz = endz, startz
        self.startz = startz
        self.endz = endz
        self.circle = []
        self.blur_radius = 30

    def find_center(self, z, show=False):
        """
        :param z: z-level
        :return: [x,y] coordinate of the beam from the image captured at that z-level
        """
        img=self.getimage(z)
        blur_img = filters.gaussian_filter(img, (self.blur_radius, self.blur_radius))
        centroid_int = np.unravel_index(blur_img.argmax(), blur_img.shape)
        if show:
            plt.imshow(img)
            plt.scatter(centroid_int[1], centroid_int[0], c='r', s=8)
        return [centroid_int[1], centroid_int[0]]

    def find_sharpness_dct(self, z):
        """
        :param z: z-level
        :return: DCT peak of the beam from the image captured at that z-level
        """
        dct_img = fftpack.dct(filters.gaussian_filter(self.getimage(z), (self.blur_radius, self.blur_radius)))
        return np.max(abs(dct_img))

    # def find_dct_xy(self, z):

    # """
    # :param z: z-level
    # :return: DCT peak and [x,y] coordinate of the beam from the image captured at that z-level
    # """
    # return np.insert(self.find_center(z), 0, self.find_sharpness_dct(z))

    def kriging_dct_focus(self, z_pts=3, thetamin=1e-5, thetamax=1e-2, show=False):
        """
        There are 2 main steps in this function.
        1: Auto-adjust the search range until there is a local minimum within the search range, while preserving the search range
        2: Estimate the x,y,z coord of the focus of the beam
            2.1: Build Kriging model for DCT, x and y coords as a function of z-level
            2.2: Estimate the z-level of focus by taking the minimum of DCT
            2.3: Predict the x,y values of that z-level
        :param z_pts: number of z-levels imaged to determine the focus (by Kriging)
        :param thetamin: lower bound of theta to build the Kriging model, don't need to change usually
        :param thetamax: upper bound of theta to build the Kriging model, should be increased when z_pts increases,
                         meaning less restrained model to prevent over-fitting
        :param show: plot DCT, x and y (and its Kriging fitting and predicted focus point) as a function of z-level
        :return: the coord x,y,z
        """
        if z_pts < 3:
            print('at least 3 points are needed')
            z_pts = 3
        search_z = np.linspace(self.startz, self.endz, num=z_pts)
        search_z = np.asarray(search_z)
        dct_peaks = []
        for z in search_z:
            dct_peaks.append(self.find_sharpness_dct(z))
        dct_peaks = np.array(dct_peaks)
        search_interval = (self.endz - self.startz) / (z_pts - 1)

        # do while loop: update start and end until a local minimum is captured
        # by polyfit and re-sample around the fitted focus
        while True:
            print(search_z)
            print(dct_peaks)
            if all(np.diff(dct_peaks) > 0) or all(np.diff(dct_peaks) < 0):
                print('focus out of range')
                fit = np.polyfit(search_z, dct_peaks, 2)
                polyfit_z_focus = -fit[1] / (2 * fit[0])
                # search_z = [polyfit_z_focus - search_interval, polyfit_z_focus, polyfit_z_focus + search_interval]
                search_z = search_z-(self.startz + self.endz)/2 + polyfit_z_focus
                dct_peaks = [*[self.find_sharpness_dct(z) for z in search_z]]
            else:
                break

        # build model
        z_pred = np.linspace(search_z[0], search_z[-1], num=1000)
        z_pred_2d = np.hstack((np.vstack(z_pred), np.vstack(z_pred)))  # make a 2D input for prediction
        search_z_2d = np.hstack((np.vstack(search_z), np.vstack(search_z)))  # make a 2D input for building mdoel
        sharpnesses = np.zeros(len(search_z), dtype=np.float)
        for i, z in enumerate(search_z):
            sharpnesses[i] = self.find_sharpness_dct(z)

        # Use DCT sharpness to find the most in focus Z plane
        krig_dct = kriging(search_z_2d, sharpnesses, testPoints=25, thetamin=thetamin, pmin=2,
                           pmax=2)  # TODO change var name!
        krig_dct.thetamax = thetamax
        krig_dct.train(optimizer='ga')
        dct_predict = np.zeros(len(z_pred), dtype=np.float)
        for i, pt in enumerate(z_pred_2d):
            dct_predict[i] = krig_dct.predict(pt)
        focus_arg = dct_predict.argmin()

        # overlay sampling points, prediction curve and focus point
        if show:
            plt.figure(figsize=(5, 5))
            plt.scatter(z_pred, dct_predict, c='r', s=2, label='fit by kriging')
            plt.scatter(z_pred[focus_arg], dct_predict[focus_arg], c='g', s=64, label='focus point')
            plt.scatter(search_z, sharpnesses, c='b', s=64, label='sampling points')
            plt.title('DCT peak vs z')
            plt.legend()
            plt.show()

        # take an image at that Z plane, and find the XY center
        return np.append(self.find_center(z_pred[focus_arg]), z_pred[focus_arg])


class Calib3D(object):
    def __init__(self, init_z, mode, img_size1=[[0, 512], [0, 512], [0, 512]],
                 img_size2=[[0, 1024], [0, 1024], [0, 1024]]):
        # img_size: 1=before transform, 2=after transform
        self.img_size1 = np.array(img_size1)
        self.img_range1 = self.img_size1[:, 1] - self.img_size1[:, 0]
        self.img_size2 = np.array(img_size2)
        self.img_range2 = self.img_size2[:, 1] - self.img_size2[:, 0]
        self.init_z = np.array(init_z)
        self.mode = mode  # 'simulation' or 'microscope'
        self.points_2_noisy = []
        self.points_2_ideal = []
        self.points_1 = []
        self.xyz_kriging_model = []
        self.n = 0

    def kriging_3d_build(self, points_1, points_2_fitting, thetamin=1e-5, thetamax=1e0, pmin=2, pmax=2):
        """
        :param points_1:            known coords before transformation
        :param points_2_fitting:    known coords after transformation
        :return: predicted coords after transformation
        """
        # build kriging model
        self.n = np.shape(points_2_fitting)[1]
        for i in range(self.n):
            #  building Kriging model
            self.xyz_kriging_model.append(regression_kriging(points_1, points_2_fitting[:, i], testPoints=250))
            self.xyz_kriging_model[-1].thetamin = thetamin
            self.xyz_kriging_model[-1].thetamax = thetamax
            self.xyz_kriging_model[-1].pmin = pmin
            self.xyz_kriging_model[-1].pmax = pmax
            self.xyz_kriging_model[-1].train(optimizer='ga')
            self.xyz_kriging_model[-1].snapshot()
        print('done building Kriging model.')
        return self.xyz_kriging_model

    def kriging_3d_predict(self, points_1_pred, points_2_ideal_pred=[], show=0):
        """
        :param points_1_pred:       coords that we want to predict
        :param points_2_ideal_pred: ideal coords after transformation, used to evaluate the prediction
        :return: predicted coords after transformation
        """
        # predict by using kriging model
        if self.n == 0:
            print('error: model not built yet.')
            return None
        krig_pred = [[], [], []]
        for i in range(self.n):
            for pt in points_1_pred:
                krig_pred[i].append(self.xyz_kriging_model[i].predict(pt))
        krig_pred = np.array(krig_pred)

        if show != 0:
            fig = plt.figure(figsize=(8, 5))
            ax = fig.add_subplot(111, projection='3d')
            if points_2_ideal_pred != []:
                ax.scatter(points_2_ideal_pred[:, 0], points_2_ideal_pred[:, 1], points_2_ideal_pred[:, 2], c='b',
                           label='ideal_transformed', s=show)
            ax.scatter(krig_pred[0], krig_pred[1], krig_pred[2], c='r', label='kriging', s=show)
            ax.set_xlabel('x')
            ax.set_ylabel('y')
            ax.set_zlabel('z')
            ax.legend()
            plt.show()
            if points_2_ideal_pred != []:
                distance_kde(krig_pred.T, points_2_ideal_pred, show=True)
        return krig_pred.T

    def scan_point(self, coord, z_range, guess):
        """
        :param coord: coord before transformation that we want to scan
        :param z_range: search range of z-level
        :param guess: rough guess of z-level, will be auto-readjust until the focus is captured
        :return: x,y,z
        """
        if self.mode == 'simulation':
            idx = np.hstack(np.where(np.prod(self.points_1 == coord, axis=-1)))[0]
            # print('idx: '+str(idx))
            get_image = SimImg(self.img_size2, self.points_2_noisy[idx]).sim_image
        else:
            print('still working on this part')
            # how to set z level ?????
            # I want to have a method that grab image at zlevel
            # set_objective_Z(zlevel)
            # get_image = CameraHandle.grab_image(zlevel)
        getfocus = AutoFocus(get_image, -z_range / 2 + guess, z_range / 2 + guess)
        return getfocus.kriging_dct_focus()

    def scan_points(self, points_1, z_range=20):
        """
        1. Scan the 8 corners
        2. Build an initial Kriging model to roughly predict the rest of the coords
        3. Scan the rest of the points
        :param points_1: coords before transformation
        :param z_range: range of z-level for finding focus
        :return: coords after transformation
        """
        # scan all points and get accurate (x,y,z)
        self.points_1 = points_1
        if self.mode == 'simulation':
            self.points_2_noisy, self.points_2_ideal = generate_points_3d(points_1, noise_percentage=0.2, show=True)
        corners_idx = np.hstack(np.where(np.prod(self.points_1 == i, axis=-1)) for i in mesh_3d(2, self.img_size1))[0]
        print('corners_idx: ' + str(corners_idx))
        # scan 8 corners
        corners = []
        print('ideal_init_z: ' + str(self.points_2_noisy[corners_idx, 2]))
        print('rough_init_z: ' + str(self.init_z))
        for j in range(np.shape(corners_idx)[0]):
            i = corners_idx[j]
            corners.append(self.scan_point(points_1[i], z_range, self.init_z[j]))
        corners = np.array(corners)
        print(points_1[corners_idx], corners, points_1, self.points_2_ideal)
        points_2_initial_guess = self.kriging_3d_predict(points_1[corners_idx], corners, points_1, self.points_2_ideal,
                                                         show=64)

        # # skip scanning 8 corners
        # print('skip scanning 8 points')
        # points_2_initial_guess = self.kriging_3d_predict(self.points_1[corners_idx], self.points_2_noisy[corners_idx],
        #                                         points_1, self.points_2_ideal, show=64)
        #
        # # print('init guess: ' + str(points_2_initial_guess[:, 2]))
        # # print('actual z: ' + str(self.points_2_noisy[:, 2]))

        # scan n**3 points
        print('fine scan')
        points_2_fine = []
        for i in range(np.shape(points_2_initial_guess)[0]):
            print('i: ' + str(i))
            points_2_fine.append(self.scan_point(points_1[i], z_range, points_2_initial_guess[i, 2]))
        points_2_fine = np.array(points_2_fine)

        # # skip scanning n**3 points
        # print('skip fine scanning')
        # points_2_fine = self.points_2_noisy

        return points_2_fine
